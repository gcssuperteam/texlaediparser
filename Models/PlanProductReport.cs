﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TexlaEDIParser.Models
{
    public class PlanProductReport
    {
        public string File { get; set; }
        public string MessageType { get; set; }
        public string Customer { get; set; }
        public int HeaderWarningTotalCount { get; set; }
        public decimal HeaderTotalDiff { get; set; }
        public decimal HeaderTotalChange { get; set; }
        public List<PlanProductSum> PlanProductSumList { get; set; }
    }
    public class PlanProductSum
    {
        public int PlanProductId { get; set; }
        public string ProductNo { get; set; }
        public string ProductNoInFile { get; set; }
        public string ProductName { get; set; }
        public string Note { get; set; }
        public string Warning { get; set; }
        public decimal CurrentOrderAmount { get; set; }
        public decimal CurrentHistoryAmount { get; set; }
        public decimal CurrentTotAmount { get; set; }
        public decimal PlanAmountBeforeTomorrow { get; set; }
        public decimal PlanAmount { get; set; }
        public decimal PlanTotalAMount { get; set; }
        public decimal DiffBeforeMatch { get; set; }
        public decimal MatchAmountAfterUpdate { get; set; }
        public decimal SumDiff { get; set; }
    }
}
