﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TexlaEDIParser.Models
{
    public partial class TexlaEDIContext : DbContext
    {
        public TexlaEDIContext()
        {
        }

        public TexlaEDIContext(DbContextOptions<TexlaEDIContext> options)
            : base(options)
        {
        }

        public virtual DbSet<GarpHistoryBatch> GarpHistoryBatch { get; set; }
        public virtual DbSet<GarpOrderBatch> GarpOrderBatch { get; set; }
        public virtual DbSet<GarpUpdateLog> GarpUpdateLog { get; set; }
        public virtual DbSet<Match> Match { get; set; }
        public virtual DbSet<PlanDetail> PlanDetail { get; set; }
        public virtual DbSet<PlanFile> PlanFile { get; set; }
        public virtual DbSet<PlanProduct> PlanProduct { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string con = GenericFunctions.GetSettingsValue("SqlDb");
                optionsBuilder.UseSqlServer(con);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<GarpHistoryBatch>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DeliverDate).HasColumnType("datetime");

                entity.Property(e => e.OrderNo).HasMaxLength(6);

                entity.Property(e => e.ProductNo).HasMaxLength(13);

                entity.HasOne(d => d.PlanProduct)
                    .WithMany(p => p.GarpHistoryBatch)
                    .HasForeignKey(d => d.PlanProductId)
                    .HasConstraintName("FK_GarpHistoryBatch_PlanProduct");
            });

            modelBuilder.Entity<GarpOrderBatch>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DeliverDate).HasColumnType("datetime");

                entity.Property(e => e.OrderNo).HasMaxLength(6);

                entity.Property(e => e.ProductNo).HasMaxLength(13);

                entity.HasOne(d => d.PlanProduct)
                    .WithMany(p => p.GarpOrderBatch)
                    .HasForeignKey(d => d.PlanProductId)
                    .HasConstraintName("FK_GarpOrderBatch_PlanProduct");
            });

            modelBuilder.Entity<GarpUpdateLog>(entity =>
            {
                entity.Property(e => e.Action).HasMaxLength(20);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.ResultFromGarp).HasMaxLength(500);

                entity.HasOne(d => d.Match)
                    .WithMany(p => p.GarpUpdateLog)
                    .HasForeignKey(d => d.MatchId)
                    .HasConstraintName("FK_GarpUpdateLog_GarpUpdateLog");
            });

            modelBuilder.Entity<Match>(entity =>
            {
                entity.Property(e => e.Action).HasMaxLength(20);

                entity.Property(e => e.AmountInGarp).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.AmountPlanned).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(500);

                entity.Property(e => e.ProductNo).HasMaxLength(50);

                entity.Property(e => e.Warning).HasMaxLength(500);
            });

            modelBuilder.Entity<PlanDetail>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DeliverDate).HasColumnType("datetime");

                entity.HasOne(d => d.PlanProduct)
                    .WithMany(p => p.PlanDetail)
                    .HasForeignKey(d => d.PlanProductId)
                    .HasConstraintName("FK_PlanDetail_PlanProduct");
            });

            modelBuilder.Entity<PlanFile>(entity =>
            {
                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.CustomerNo).HasMaxLength(10);

                entity.Property(e => e.CustomersOrderNo).HasMaxLength(30);

                entity.Property(e => e.FileDate).HasColumnType("datetime");

                entity.Property(e => e.FileName).HasMaxLength(50);

                entity.Property(e => e.Folder).HasMaxLength(100);

                entity.Property(e => e.MessageType).HasMaxLength(10);

                entity.Property(e => e.PortNo).HasMaxLength(30);

                entity.Property(e => e.ReadedDate).HasColumnType("datetime");

                entity.Property(e => e.StorageNo).HasMaxLength(30);

                entity.Property(e => e.Text).HasColumnType("text");
            });

            modelBuilder.Entity<PlanProduct>(entity =>
            {
                entity.Property(e => e.PartConsignmentNo).HasMaxLength(30);

                entity.Property(e => e.ProductName).HasMaxLength(20);

                entity.Property(e => e.ProductNo).HasMaxLength(13);

                entity.Property(e => e.ProductNoInFile).HasMaxLength(30);

                entity.Property(e => e.Text1).HasMaxLength(30);

                entity.Property(e => e.Text2).HasMaxLength(30);

                entity.Property(e => e.Text3).HasMaxLength(10);

                entity.Property(e => e.Text4).HasMaxLength(25);

                entity.Property(e => e.Text5).HasMaxLength(25);

                entity.Property(e => e.Text6).HasMaxLength(20);

                entity.HasOne(d => d.PlanFile)
                    .WithMany(p => p.PlanProduct)
                    .HasForeignKey(d => d.PlanFileId)
                    .HasConstraintName("FK_PlanProduct_PlanFile");
            });
        }
    }
}
