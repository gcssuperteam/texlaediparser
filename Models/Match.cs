﻿using System;
using System.Collections.Generic;

namespace TexlaEDIParser.Models
{
    public partial class Match
    {
        public Match()
        {
            GarpUpdateLog = new HashSet<GarpUpdateLog>();
        }

        public long? Id { get; set; }
        public DateTime? Date { get; set; }
        public string ProductNo { get; set; }
        public decimal? AmountPlanned { get; set; }
        public int? Type { get; set; }
        public int? PlanDetailId { get; set; }
        public int? GarpOrderBatchId { get; set; }
        public int? PlanProductId { get; set; }
        public decimal? AmountInGarp { get; set; }
        public string Action { get; set; }
        public string Note { get; set; }
        public string Warning { get; set; }

        public virtual ICollection<GarpUpdateLog> GarpUpdateLog { get; set; }
    }
}
