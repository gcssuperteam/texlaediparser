﻿using System;
using System.Collections.Generic;

namespace TexlaEDIParser.Models
{
    public partial class GarpUpdateLog
    {
        public int Id { get; set; }
        public int? PlanProductId { get; set; }
        public long? MatchId { get; set; }
        public string Action { get; set; }
        public string ResultFromGarp { get; set; }
        public DateTime? Date { get; set; }
        public bool? UpdateState { get; set; }

        public virtual Match Match { get; set; }
    }
}
