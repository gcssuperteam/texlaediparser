﻿using System;
using System.Collections.Generic;

namespace TexlaEDIParser.Models
{
    public partial class PlanFile
    {
        public PlanFile()
        {
            PlanProduct = new HashSet<PlanProduct>();
        }

        public int Id { get; set; }
        public string FileName { get; set; }
        public string Folder { get; set; }
        public DateTime? ReadedDate { get; set; }
        public DateTime? FileDate { get; set; }
        public string Text { get; set; }
        public string CustomerNo { get; set; }
        public int? CustomerTransportTime { get; set; }
        public string CustomerName { get; set; }
        public bool? UpdatedToGarp { get; set; }
        public string MessageType { get; set; }
        public string CustomersOrderNo { get; set; }
        public string PortNo { get; set; }
        public string StorageNo { get; set; }
        public string DeliverCustomerName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipCity { get; set; }
        public string CountryCode { get; set; }



        public virtual ICollection<PlanProduct> PlanProduct { get; set; }
    }
}
