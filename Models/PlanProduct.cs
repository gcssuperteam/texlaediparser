﻿using System;
using System.Collections.Generic;

namespace TexlaEDIParser.Models
{
    public partial class PlanProduct
    {
        public PlanProduct()
        {
            GarpHistoryBatch = new HashSet<GarpHistoryBatch>();
            GarpOrderBatch = new HashSet<GarpOrderBatch>();
            PlanDetail = new HashSet<PlanDetail>();
        }

        public int Id { get; set; }
        public string ProductNo { get; set; }
        public int? PlanFileId { get; set; }
        public string ProductName { get; set; }
        public string ProductNoInFile { get; set; }
        public string BuyerProductNo { get; set; } // Only used with Gabriel
        public string PartConsignmentNo { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string Text3 { get; set; }
        public string Text4 { get; set; }
        public string Text5 { get; set; }
        public string Text6 { get; set; }
        public string GenericText { get; set; }

        public virtual PlanFile PlanFile { get; set; }
        public virtual ICollection<GarpHistoryBatch> GarpHistoryBatch { get; set; }
        public virtual ICollection<GarpOrderBatch> GarpOrderBatch { get; set; }
        public virtual ICollection<PlanDetail> PlanDetail { get; set; }
    }
}
