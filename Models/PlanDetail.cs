﻿using System;
using System.Collections.Generic;

namespace TexlaEDIParser.Models
{
    public partial class PlanDetail
    {
        public int Id { get; set; }
        public int? PlanProductId { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? DeliverDate { get; set; }
        public int? Type { get; set; }

        public virtual PlanProduct PlanProduct { get; set; }
    }
}
