﻿using GIS_Dto;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TexlaEDIParser.Models;
using TexlaEDIParser.MsgFunc;

namespace TexlaEDIParser
{
    public static class BusinessLayer
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(Program));


        public static void businessEntryPoint(string[] args)
        {

            try
            {
                readFiles();
            }
            catch(Exception e)
            {
                logger.Error("Error in businessEntryPoint", e);
            }

            //if (args.Length > 0) // If any args is pressent, then we do not red any new files
            //{
            //    readSqlData(args);
            //}
            //else 
            //{
            //    readFiles();
            //}
        }

        /// <summary>
        /// Reads EDI files and adds to Garp/SQL depending of EDI type
        /// </summary>
        private static void readFiles()
        {
            try
            {
                logger.Debug("Running match");


                List<string> fileList = new List<string>();

                // Get all files in input directory
                fileList = FileIOHandler.GetFileList();

                // Loop all files in the list we got
                foreach (string file in fileList)
                {
                    // Parse current file
                    PlanFile planfile = FileParser.ParseFile(file);
                    
                    if(planfile != null)
                    {
                        // Add plan to SqlDb
                        planfile = SqlDb.AddPlanFile(planfile);

                        if (planfile.MessageType == "DELFOR" || planfile.MessageType == "DELINS")
                        {
                            Delfor.runDelfor(planfile);
                        }
                        else if (planfile.MessageType == "DELJIT" || planfile.MessageType == "ORDERS")
                        {
                            Deljit.runDeljit(planfile);
                        }
                    }
                    else
                    {
                        logger.Debug("Could not handle file " + planfile + " for some reason");
                    }
                }


            }
            catch (Exception e)
            {
                logger.Error("Errir while reading files in readFiles()", e);
            }

        }

        ///// <summary>
        ///// Reads Sql data that been added from files in previouse execution. This only apply for DELFOR files 
        ///// that are already readed
        ///// </summary>
        ///// <param name="args"></param>
        //private static void readSqlData(string [] args)
        //{
        //    try
        //    {
        //        Console.WriteLine("Running with args: " + args[0]);
        //        logger.Debug("Running with args: " + args[0]);

        //        // If "runall" we run all productplans that not alreade are executed against Garp
        //        if (args[0].Trim() == "runall")
        //        {
        //            Delfor.ExecuteAllNotAlreadyExecutedPlanFilesOnGarp();
        //        }
        //        else // Here we expect a ProductPlan ID ar argument
        //        {
        //            Console.WriteLine("Executing ExecutePlanFileOnGarp()");
        //            Delfor.ExecutePlanFileOnGarp(int.Parse(args[0].Trim()));
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //}

        //public static void runMatch()
        //{
        //    List<string> fileList = new List<string>();

        //    try
        //    {

        //        // Loop all files in the list we got
        //        foreach (string file in fileList)
        //        {
        //            // Parse current file
        //            PlanFile planfile = FileParser.ParseFile(file);
                    
        //            // Add plan to SqlDb
        //            planfile = SqlDb.AddPlanFile(planfile);

        //            // Add all corresponding order in this planfile to SqlDb
        //            GarpDb.AddGarpOrderList(planfile);

        //            // Add all corresponding history in this planfile to SqlDb
        //            GarpDb.AddGarpHistoryList(planfile);

        //            // Execute plans in current file
        //            matchPlansInFile(planfile);

        //            // When done, move file to outputfolder
        //            FileIOHandler.MoveFileToOutput(file);

        //            if(GenericFunctions.GetSettingsValue("MailReceipents") != "")
        //            {
        //                PlanProductReport report = SqlDb.GetPlanProductSumList(planfile.Id, true);
        //                sendMail(report);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("Error in BusinessLayer.runMatch()", e);
        //    }
        //}

    }
}
