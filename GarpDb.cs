﻿using GIS_Dto;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using TexlaEDIParser.Models;

namespace TexlaEDIParser
{
    public static class GarpDb
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(Program));
        static List<Customer> mCustomerList = new List<Customer>();
        static List<Product> mProductList = new List<Product>();

        public static void AddGarpOrderList(PlanFile planfile)
        {
            try
            {
                List<OrderHead> orderOnCustomer = GarpDb.GetOrderListOnCustomer(planfile.CustomerNo);

                foreach (PlanProduct planproduct in planfile.PlanProduct)
                {
                    List<OrderRow> order = orderOnCustomer.SelectMany(o => o.OrderRows.Where(r => r.ProductNo == planproduct.ProductNo && r.DeliverState == "0"))?.ToList();
                    order = FilterOutCorrectionRows(order);

                    List<GarpOrderBatch> batchList = new List<GarpOrderBatch>();

                    foreach (OrderRow or in order)
                    {
                        GarpOrderBatch gob = new GarpOrderBatch();

                        gob.Amount = or.Amount;
                        gob.DeliverDate = GenericFunctions.getDateFromStr(or.PreferedDeliverDate);
                        gob.OrderNo = or.OrderNo;
                        gob.RowNo = or.RowNo;
                        gob.ProductNo = or.ProductNo;
                        gob.IsProductPlanned = or.BLF == "0" ? false : true; // Productionplanned or not?
                        gob.PlanProductId = planproduct.Id;
                        //gob.Id = null;

                        batchList.Add(gob);
                    }

                    SqlDb.AddGarpOrderList(batchList);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in BusinessLayer.executePlanOnProduct()", e);
            }
        }

        /// <summary>
        /// Function that deletes all rows with rowno greater than "1". Theese are corrections rows, and
        /// if nobody have remived and corrected this order yesterdfay, then we remove them, else they are treated as regular rows and cause some problems
        /// </summary>
        /// <param name="orderlist"></param>
        private static List<OrderRow> FilterOutCorrectionRows(List<OrderRow> orderrows)
        {
            List<OrderRow> result = new List<OrderRow>();

            try
            {
                // Get list of orderrows to delete
                List<OrderRow> orderToDelete = orderrows.Where(o => o.RowNo > 1)?.ToList();

                //List of orderrow to return, only rows with RowNo == 1
                result = orderrows.Where(o => o.RowNo == 1)?.ToList();

                // Loop through alla rows that should be deleted
                foreach (OrderRow or in orderToDelete)
                {
                    // Doublecheck so we NEVER delete anything on orderrow 1
                    if(or.RowNo != 1)
                    {
                        GIS.GISCalls.DeleteOrderRow(or);
                        logger.Debug("Deleted OrderRow: " + or.OrderNo + "-" + or.RowNo + " as an correction row");
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in BusinessLayer.DeleteCorrectionRows()", e);
            }

            return result;
        }

        public static void AddGarpHistoryList(PlanFile planfile)
        {
            try
            {
                string calculatedTransportDate = DateTime.Now.AddDays((planfile.CustomerTransportTime.HasValue ? planfile.CustomerTransportTime.Value : 0) * -1).ToString("yyMMdd");

                List<OrderRow> historyList = GarpDb.GetHistoryListOnCustomer(planfile.CustomerNo, calculatedTransportDate);

                foreach (PlanProduct planproduct in planfile.PlanProduct)
                {
                    List<OrderRow> order = historyList.Where(r => r.ProductNo == planproduct.ProductNo)?.ToList();
                    List<GarpHistoryBatch> historyBatchList = new List<GarpHistoryBatch>();

                    foreach (OrderRow or in order)
                    {
                        GarpHistoryBatch gob = new GarpHistoryBatch();

                        gob.Amount = or.Amount;
                        gob.DeliverDate = GenericFunctions.getDateFromStr(or.PreferedDeliverDate);
                        gob.OrderNo = or.OrderNo;
                        gob.RowNo = or.RowNo;
                        gob.ProductNo = or.ProductNo;

                        gob.PlanProductId = planproduct.Id;

                        historyBatchList.Add(gob);
                    }

                    SqlDb.AddGarpHistoryList(historyBatchList);
                }

            }
            catch (Exception e)
            {
                logger.Error("Error in BusinessLayer.executePlanOnProduct()", e);
            }
        }

        private static List<OrderHead> GetOrderListOnCustomer(string customerNo)
        {
            List<OrderHead> result = new List<OrderHead>();

            try
            {
                // Filter out ORDERS (Z1) and DELJIT (Z3), these are NOT preliminary orders and should not be considered in match against plan
                OrderListParam param = new OrderListParam { Index = 2, Range = customerNo, Filter = "LEF<=4;VRF!=Z1;VRF!=Z3", WithRows = true, MaxCount = 5000, ReverseReading = true };
                result = GIS.GISCalls.GetOrderByIdxList(param);
            }
            catch (Exception e)
            {
                logger.Error("Error in GarpDb.GetOrderList()", e);
            }

            return result;
        }

        //private static List<OrderRow> GetOrderList(string customerNo, string productNo)
        //{
        //    List<OrderRow> result = new List<OrderRow>();

        //    try
        //    {
        //        List<OrderHead> orderList = GIS.GISCalls.GetOrderByIdxList("2", customerNo, "LEF<=4", true, 5000, true);

        //        foreach (OrderHead oh in orderList)
        //        {
        //            foreach (OrderRow or in oh.OrderRows)
        //            {
        //                if (or.ProductNo.Trim().Equals(productNo.Trim()) && or.DeliverState.Equals("0") && (GenericFunctions.getDateFromStr(or.PreferedDeliverDate).Date.CompareTo(DateTime.Now.Date) > 0))
        //                {
        //                    result.Add(or);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("Error in GarpDb.GetOrderList()", e);
        //    }

        //    return result;
        //}

        private static List<OrderRow> GetHistoryListOnCustomer(string customerNo, string dateFrom)
        {
            List<OrderRow> result = new List<OrderRow>();

            try
            {
                Customer customer = GarpDb.GetCustomer(customerNo);
                DateTime transportDate = DateTime.Now.AddDays(GenericFunctions.getIntFromStr(customer.TransportTime) * -1);

                List<CustomerHistory> historyList = GIS.GISCalls.GetHistoryOnCustomerList(customerNo, customerNo, dateFrom, DateTime.Now.ToString("yyMMdd"));

                foreach (CustomerHistory ch in historyList)
                {
                    OrderHead oh = GIS.GISCalls.GetOrderById(ch.OrderNo, true);

                    // Filter out ORDERS (Z1) and DELJIT (Z3), these are NOT preliminary orders and should not be considered in match against plan
                    if (oh.OurReferenceId != "Z1" && oh.OurReferenceId != "Z3")
                    {
                        foreach (OrderRow or in oh.OrderRows)
                        {
                            result.Add(or);
                        }
                    }
                }

                // Now we gett all orders today
                //List<OrderHead> orderList = GIS.GISCalls.GetOrderByIdxList("2", customerNo, "LEF<=4", true, 0, true);
                //foreach (OrderHead oh in orderList)
                //{
                //    //if (oh.OrderNo == "218107")
                //    //    Console.WriteLine("");

                //    foreach (OrderRow or in oh.OrderRows)
                //    {
                //        if (or.DeliverState.Equals("0") && (GenericFunctions.getDateFromStr(or.PreferedDeliverDate).Date.CompareTo(DateTime.Now.Date)) <= 0)
                //        {
                //            result.Add(or);
                //        }
                //    }
                //}
            }
            catch (Exception e)
            {
                logger.Error("Error in GarpDb.GetHistoryList()", e);
            }

            return result;
        }

        private static List<OrderRow> GetHistoryList(string customerNo, string dateFrom, string productNo)
        {
            List<OrderRow> result = new List<OrderRow>();

            try
            {
                Customer customer = GarpDb.GetCustomer(customerNo);
                DateTime transportDate = DateTime.Now.AddDays(GenericFunctions.getIntFromStr(customer.TransportTime) * -1);

                List<CustomerHistory> historyList = GIS.GISCalls.GetHistoryOnCustomerList(customerNo, customerNo, dateFrom, DateTime.Now.ToString("yyMMdd"));

                foreach (CustomerHistory ch in historyList)
                {
                    OrderHead oh = GIS.GISCalls.GetOrderById(ch.OrderNo, true);

                    foreach (OrderRow or in oh.OrderRows)
                    {
                        if (or.ProductNo.Trim().Equals(productNo.Trim()))
                        {
                            result.Add(or);
                        }
                    }
                }

                // Now we gett all orders today
                OrderListParam param = new OrderListParam { Index = 2, Range = customerNo, Filter = "LEF<=4", WithRows = true, MaxCount = 5000, ReverseReading = true };
                List<OrderHead> orderList = GIS.GISCalls.GetOrderByIdxList(param);

                foreach (OrderHead oh in orderList)
                {
                    foreach (OrderRow or in oh.OrderRows)
                    {
                        if (or.ProductNo.Trim().Equals(productNo.Trim()) && or.DeliverState.Equals("0") && (GenericFunctions.getDateFromStr(or.PreferedDeliverDate).Date.CompareTo(DateTime.Now.Date) == 0))
                        {
                            result.Add(or);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in GarpDb.GetHistoryList()", e);
            }

            return result;
        }

        public static void ExecuteMatchOnGarp(int planproductid)
        {
            try
            {
                var matchList = SqlDb.GetMatchByPlanProduct(planproductid);

                foreach (Match match in matchList)
                {
                    if (match.Action == "add")
                    {
                        AddOrderrow(match);
                    }
                    else if (match.Action == "delete")
                    {
                        DeleteOrderrow(match);
                    }
                    else if (match.Action == "change")
                    {
                        bool update = true;
                        try
                        {
                            GarpOrderBatch gob = SqlDb.GetGarpOrderBatch(match.GarpOrderBatchId.Value);
                            if(gob.IsProductPlanned == true)
                            {
                                update = false;
                            }
                        }
                        catch { }
                        
                        if(update)
                        {
                            ChangeOrderrow(match);
                        }
                        else
                        {
                            logger.Debug("Skipped UpdateOrderRow due to ProductPlanned orderrow: " + JsonConvert.SerializeObject(match));
                        }
                        
                    }
                    else if (match.Action == "change_date")
                    {
                        ChangeOrderrow(match);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in GarpDb.AddMatchToGarp()", e);
            }
        }

        public static void ProductPlanOrderRow(OrderHead order)
        {
            try
            {
                
            }
            catch(Exception e)
            {
                logger.Error("error in ProductPlanOrderRow", e);
            }
        }

        /// <summary>
        /// Add new orderrow. Either a whole new order, or if this is a diff post we add
        /// a new orderrow on a existing orderrow
        /// </summary>
        /// <param name="match"></param>
        public static OrderHead AddOrderrow(Match match)
        {
            OrderHead oh = new OrderHead();

            try
            {
                PlanProduct planProduct = SqlDb.GetPlanProduct(match.PlanProductId.Value);
                PlanFile planFile = SqlDb.GetPlanFile(planProduct.PlanFileId.Value);
                string orderno = "";

                // If Match has a GarpOrderbacth value then this is att diff post that should add att new row on a 
                // existing order (row 2)
                if(match.GarpOrderBatchId.HasValue)
                {
                    orderno = SqlDb.GetGarpOrderBatch(match.GarpOrderBatchId.Value).OrderNo;
                }
                else
                {
                    orderno = GenericFunctions.GetSettingsValue("OrderSerie");
                }

                oh.OrderNo = orderno;
                oh.DeliverCustomerNo = planFile.CustomerNo;
                oh.IsDeliveryAddressUnique = false;
                 
                oh.OurReferenceId = GenericFunctions.getOurReferenceFromMessageType(planFile.MessageType);
                oh.PreferedDeliverDate = match.Date.Value.ToString("yyMMdd");
                
                try
                {
                    // On order on customer "Gabriel" we set orderno in OGF:TX4
                    if(oh.DeliverCustomerNo.StartsWith("435-"))
                    {
                        
                        // Changes made 2021-03-29 after mail from Ulrika
                        oh.Text4 = planFile.CustomersOrderNo;

                        // JB 230413: Added this for Gabriels ProductNo that should be saved here
                        oh.DeliverInstruction = planProduct.BuyerProductNo;

                        // 2021-03-24 Added this for customer Gabriel
                        if (oh.OrderRowZeroTexts == null)
                        {
                            oh.OrderRowZeroTexts = new List<OrderRowText>();
                        }

                        oh.OrderRowZeroTexts.Add(new OrderRowText
                        {
                            InvoiceState = "M",
                            Text = planProduct.GenericText,
                            RowNo = "0",
                            TextRowNo = "255"
                        });
                    }

                    string text1 = planProduct.Text1.PadRight(30) + planProduct.Text2.PadRight(10) + planProduct.Text3;
                    oh.Text1 = text1;

                    if (planFile.MessageType == "DELJIT")
                    {
                        string text2 = planProduct.Text4.PadRight(25) + planProduct.Text5;
                        oh.Text2 = text2;
                    }

                    string text3 = planProduct.Text6;
                    oh.Text3 = text3;
                }
                catch (Exception e)
                {
                    logger.Error("Error setting Text on OH", e);
                }

                // If productno is emty (product is not found in Garp) then we dont
                // try to add it to Garp, this should only create empty OH problem (GIS will prevent the orderrow from being created)
                if(!string.IsNullOrEmpty(planProduct.ProductNo))
                {
                    oh.OrderRows = new List<OrderRow>();
                    oh.OrderRows.Add(new OrderRow
                    {
                        OrderNo = orderno,
                        RowNo = 255,
                        ProductNo = match.ProductNo,
                        Amount = match.AmountInGarp,
                        PreferedDeliverDate = match.Date.Value.ToString("yyMMdd"),
                        WarehouseNo = "1"
                    });

                    OrderResult<OrderHead> addOrderResult = new OrderResult<OrderHead>();

                    // Depending on if this is a add of OH or if this is only a add of OrderRow
                    if (match.GarpOrderBatchId.HasValue)
                    {
                        GIS.GISCalls.AddOrderRow(oh.OrderRows);

                        // This GIS call dosent return anything, we have to asume that everything went fine...
                        addOrderResult.Succeeded = true;
                    }
                    else
                    {
                        addOrderResult = GIS.GISCalls.AddOrderHead(oh);

                        if (addOrderResult.Succeeded)
                        {
                            oh = addOrderResult.Result;
                        }

                    }


                    if (addOrderResult != null)
                    {
                        addGarpUpdateLogForAdd(match, addOrderResult);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in GarpDb.AddOrderrow()", e);
            }

            return oh;
        }

        //public static void AddOrder(OrderHead order)
        //{
        //    try
        //    {

        //    }
        //    catch(Exception e)
        //    {
        //        logger.Error("Error in AddOrder", e);
        //    }
        //}


        public static void DeleteOrderrow(Match match)
        {
            try
            {
                GarpOrderBatch gob = SqlDb.GetGarpOrderBatch(match.GarpOrderBatchId.Value);

                if (gob != null)
                {
                    // NEVER EVER DELETE PRODUCTPLANNED ORDERS !!!!!!!!!!!!!!!!!!
                    if(gob.IsProductPlanned != true)
                    {
                        OrderResult<string> deleteResult = GIS.GISCalls.DeleteOrderRow(new OrderRow
                        {
                            OrderNo = gob.OrderNo,
                            RowNo = gob.RowNo.Value
                        });

                        // If no more orderrows exists we also delete the orderhead.
                        OrderHead oh = GIS.GISCalls.GetOrderById(gob.OrderNo, true);
                        if (oh.OrderRows?.Count == 0)
                        {
                            GIS.GISCalls.DeleteOrderHead(oh);
                        }

                        addGarpUpdateLogForDelete(match, gob.OrderNo, gob.RowNo.ToString(), deleteResult);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in GarpDb.DeleteOrderrow()", e);
            }
        }

        public static void ChangeOrderrow(Match match)
        {
            try
            {
                GarpOrderBatch gob = SqlDb.GetGarpOrderBatch(match.GarpOrderBatchId.Value);
                OrderRow or = GIS.GISCalls.GetOrderRow(gob.OrderNo, gob.RowNo.Value.ToString());
                OrderHead oh = GIS.GISCalls.GetOrderById(gob.OrderNo, false);

                

                if (or != null)
                {
                    // Get rid of unnecessary fields
                    or.Product = null;
                    or.Textrows = null;

                    or.Amount = match.AmountInGarp;
                    or.PreferedDeliverDate = match.Date.Value.ToString("yyMMdd");
                    or.WarehouseNo = "1";
                    
                    GIS.GISCalls.UpdateOrderRow(or);
                }


                // Also change date and time on orderhead to show that this order i schanged
                if(oh != null)
                {
                    // Get rid of unnecessary fields
                    oh.DeliverCustomer = null;

                    oh.Date = DateTime.Now.ToString("yyMMdd");
                    oh.Order_ttmm = DateTime.Now.ToString("HHmm");
                    oh.PreferedDeliverDate = match.Date.Value.ToString("yyMMdd");
                    oh.OurReferenceId = "Z2";

                    GIS.GISCalls.UpdateOrderHead(oh);
                }

                addGarpUpdateLogForChange(match, gob.OrderNo, gob.RowNo.ToString());
            }
            catch (Exception e)
            {
                logger.Error("Error in GarpDb.AddOrderrow()", e);
            }
        }

        //public static List<Product> GetProductList()
        //{
        //    List<Product> result = new List<Product>();

        //    try
        //    {

        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("Error in GarpDb.GetProductList()", e);
        //    }

        //    return result;

        //}

        public static Product GetProduct(string id)
        {
            Product result = new Product();

            try
            {
                //result = GIS.GISCalls.GetProductById(GenericFunctions.GetSettingsValue("GISToken"), id);

                // If we ever had search for products before, do a serch and save to our list. 
                if (mProductList?.Count == 0)
                {
                    mProductList = GIS.GISCalls.GetProductList("1", "*", "TYP!=z");
                }

                result = mProductList.Find(c => c.ProductNo == id);

                // If product not match productno search for product in Text 8
                if (string.IsNullOrEmpty(result?.ProductNo))
                {
                    List<Product> newTextList = mProductList.Where(t => t.ProductText != null).ToList();
                    // Check if id is in Text 8 field on product
                    result = newTextList.Find(c => c.ProductText[7]?.Value == id);
                }

                // Check and log if product is not found
                if(result == null)
                {
                    logger.Debug("Could not find product: " + id + " in Garp");
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in GarpDb.GetProduct() while searching for product: " + id, e);
            }

            return result;
        }

        //public static List<Customer> GetCustomerList()
        //{
        //    List<Customer> result = new List<Customer>();

        //    try
        //    {

        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("Error in GarpDb.GetCustomerList()", e);
        //    }

        //    return result;

        //}

        public static Customer GetCustomer(string id)
        {
            Customer result = new Customer();

            try
            {
                //result = GIS.GISCalls.GetCustomerById(id);
                //if (string.IsNullOrEmpty(result?.CustomerNo))
                //{
                //}

                // If we ever had search for customers before, do a serch and save to our list. 
                // This is because we can't filter on the EANRecipentAddress field (it's in the KB table not the KA table in GIS)
                if (mCustomerList?.Count == 0)
                {
                    mCustomerList = GIS.GISCalls.GetCustomerList("1", "*", "KTY!=z");
                }

                result = mCustomerList.Find(c => c.EANRecipientAddress == id);
            }
            catch (Exception e)
            {
                logger.Error("Error in GarpDb.GetCustomerList()", e);
            }

            return result;
        }

        private static void addGarpUpdateLogForAdd(Match match, OrderResult<OrderHead> addOrderResult)
        {
            GarpUpdateLog log = new GarpUpdateLog();

            try
            {
                if(addOrderResult.Succeeded == true)
                {
                    OrderHead addedOH = addOrderResult.Result;

                    log.Date = DateTime.Now;
                    log.Action = match.Action;
                    log.MatchId = match.Id;
                    log.PlanProductId = match.PlanProductId;

                    if (addedOH.OrderRows.Find(r => r.ProductNo == match.ProductNo.Trim() && r.Amount == match.AmountInGarp && r.PreferedDeliverDate == match.Date.Value.ToString("yyMMdd")) != null)
                    {
                        log.ResultFromGarp = "Added orderno: " + addedOH.OrderNo;
                        log.UpdateState = true;
                    }
                    else
                    {
                        log.ResultFromGarp = "Orderrow was NOT created on order " + addedOH.OrderNo;
                        log.UpdateState = false;
                    }
                }
                else
                {
                    log.Date = DateTime.Now;
                    log.Action = match.Action;
                    log.MatchId = match.Id;
                    log.PlanProductId = match.PlanProductId;

                    log.ResultFromGarp = addOrderResult.Message;
                    log.UpdateState = false;
                }

            }
            catch (Exception e)
            {
                logger.Error("Error in GarpDb.addGarpUpdateLogForAdd()", e);
                log.ResultFromGarp = "Error: " + e.Message;
                log.UpdateState = false;
            }

            SqlDb.AddGarUpdateLog(log);
        }

        private static void addGarpUpdateLogForChange(Match match, string onr, string row)
        {
            GarpUpdateLog log = new GarpUpdateLog();

            try
            {
                Console.WriteLine("Executing addGarpUpdateLogForChange()");

                OrderRow changedOrderrow = GIS.GISCalls.GetOrderRow(onr, row);

                log.Date = DateTime.Now;
                log.Action = match.Action;
                log.MatchId = match.Id;
                log.PlanProductId = match.PlanProductId;

                if (changedOrderrow != null)
                {
                    if (changedOrderrow.ProductNo == match.ProductNo && changedOrderrow.Amount == match.AmountInGarp && changedOrderrow.PreferedDeliverDate == match.Date.Value.ToString("yyMMdd"))
                    {
                        log.ResultFromGarp = "Orderrow changed: " + onr + "-" + row;
                        log.UpdateState = true;
                    }
                    else
                    {
                        log.ResultFromGarp = "Orderrow was NOT changed: " + onr + "-" + row;
                        log.UpdateState = false;
                    }
                }
                else
                {
                    log.ResultFromGarp = "No order was created";
                    log.UpdateState = false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in addGarpUpdateLogForChange() : " + e.Message);
                logger.Error("Error in GarpDb.addGarpUpdateLogForChange()", e);
                log.ResultFromGarp = "Error: " + e.Message;
                log.UpdateState = false;
            }

            SqlDb.AddGarUpdateLog(log);
        }

        private static void addGarpUpdateLogForDelete(Match match, string onr, string row, OrderResult<string> deleteResult)
        {
            GarpUpdateLog log = new GarpUpdateLog();

            try
            {
                OrderRow deletedOrderrow = GIS.GISCalls.GetOrderRow(onr, row);

                log.Date = DateTime.Now;
                log.Action = match.Action;
                log.MatchId = match.Id;
                log.PlanProductId = match.PlanProductId;

                if (deletedOrderrow == null)
                {
                    log.ResultFromGarp = "Orderrow was deleted:  " + onr+"-"+row;
                    log.UpdateState = true;
                }
                else
                {
                    if(string.IsNullOrEmpty(deleteResult.Message))
                    {
                        log.ResultFromGarp = "Orderrow was NOT deleted: " + onr + "-" + row;
                    }
                    else
                    {
                        log.ResultFromGarp = deleteResult.Message;
                    }
                    log.UpdateState = false;
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in GarpDb.addGarpUpdateLogForDelete()", e);
                log.ResultFromGarp = "Error: " + e.Message;
                log.UpdateState = false;
            }

            SqlDb.AddGarUpdateLog(log);
        }
    }
}
