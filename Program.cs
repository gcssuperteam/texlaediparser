﻿using System;
using System.IO;
using System.Reflection;
using log4net;
using log4net.Config;
using Microsoft.Extensions.Configuration;

namespace TexlaEDIParser
{
    class Program
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            try
            {
                var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
                XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

                Console.WriteLine("Upp and running!");
                logger.Debug("Running");

                BusinessLayer.businessEntryPoint(args);


                //if (args.Length > 0)
                //{
                //    Console.WriteLine("Running with args: " + args[0]);
                //    logger.Debug("Running with args: " + args[0]);

                //    // If "runall" we run all productplans that not alreade are executed against Garp
                //    if (args[0].Trim() == "runall")
                //    {
                //        BusinessLayer.ExecuteAllNotAlreadyExecutedPlanFilesOnGarp();
                //    }
                //    else // Here we expect a ProductPlan ID ar argument
                //    {
                //        Console.WriteLine("Executing ExecutePlanFileOnGarp()");
                //        BusinessLayer.ExecutePlanFileOnGarp(int.Parse(args[0].Trim()));
                //    }
                //}
                //else
                //{
                //    logger.Debug("Running match");
                //    BusinessLayer.runMatch();
                //}
            }
            catch (Exception e)
            {
                logger.Error("Error starting application", e);
            }
        }
    }
}
