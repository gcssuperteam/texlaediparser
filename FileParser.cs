﻿using GIS_Dto;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using TexlaEDIParser.Models;

namespace TexlaEDIParser
{
    public static class FileParser
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(Program));

        public static PlanFile ParseFile(string file)
        {
            PlanFile result = new PlanFile();

            try
            {
                List<string> lines = readFile(file);
                List<string> msgDocHead = extractMsgDocHead(lines);
                List<string[]> splittedLines = splitAndsortLines(lines);


                // If CustomerNo is not found in "DOC" post then get it from th "HEAD" post
                //if (string.IsNullOrEmpty(customerNo))
                //{
                //    customerNo = msgDocHead[2].Split(",")[12];
                //}


                // New directive 2020-05-14. We should never read customer from the DOC post
                string customerNo = msgDocHead[2].Split(",")[12];//msgDocHead[1].Split(",")[5];

                Customer customer = GarpDb.GetCustomer(customerNo);

                if(customer != null)
                {
                    // Abort if customer is not found in Garp
                    if (!string.IsNullOrEmpty(customer.CustomerNo))
                    {
                        result.PlanProduct = getPlanProductList(splittedLines);
                        result.ReadedDate = DateTime.Now;
                        result.Text = File.ReadAllText(file);
                        result.FileDate = File.GetLastAccessTime(file);
                        result.Folder = Path.GetDirectoryName(file);
                        result.FileName = Path.GetFileName(file);
                        result.CustomerNo = customer.CustomerNo;
                        result.CustomerName = customer.Name;
                        result.CustomersOrderNo = msgDocHead[1].Split(",")[1];
                        
                        try
                        {
                            if (msgDocHead[2].Split(",").Length >= 47)
                            {
                                UserErpInfo uei = GIS.GISCalls.GetERPInfo("SYS");

                                result.DeliverCustomerName = msgDocHead[2].Split(",")[44].Substring(0, Math.Min(msgDocHead[2].Split(",")[44].Length, 30)); // Trunctade if nessesarry
                                result.Address1 = msgDocHead[2].Split(",")[45].Substring(0, Math.Min(msgDocHead[2].Split(",")[45].Length, 30));
                                //result.Address2 = msgDocHead[2].Split(",")[45];
                                result.ZipCity = msgDocHead[2].Split(",")[46].Substring(0, Math.Min(msgDocHead[2].Split(",")[46].Length, 30));
                                result.CountryCode = msgDocHead[2].Split(",")[48].Substring(0, Math.Min(msgDocHead[2].Split(",")[48].Length, 2));

                                // If Garps countrycode is the same as coutrycode in file (deliver to customer in same country) countrycode in Garp should always be blank, så we fix this
                                if (uei.CountryCode == result.CountryCode)
                                {
                                    result.CountryCode = "";
                                }
                            }
                        }
                        catch(Exception e)
                        {
                            logger.Error("Error setting customerunique deliver address", e);
                        }

                        result.CustomerTransportTime = GenericFunctions.getIntFromStr(customer.TransportTime);
                        result.MessageType = msgDocHead[0].Split(",")[1];
                    }
                    else
                    {
                        logger.Info("Customer not found in Garp, customerno in file was: " + customerNo + ". We are aborting!");
                    }
                }
                else
                {
                    logger.Debug("Could not find Customer " + customerNo + " in Garp");
                    result = null;
                }


            }
            catch (Exception e)
            {
                logger.Error("Error in BusinessLayer.parseFile()", e);
                result = null;
            }

            return result;
        }

        private static List<string> readFile(string file)
        {
            List<string> result = new List<string>();
            Encoding oem = CodePagesEncodingProvider.Instance.GetEncoding(1252);

            try
            {
                string[] lines = File.ReadAllLines(file, oem);

                foreach(string line in lines)
                {
                    if(!line.StartsWith("EOM"))
                    {
                        result.Add(line.Replace("\"", "").Replace(@"\", ""));
                    }
                    
                }
                //result = lines.Where(l=>!l[0]. ("EOM")).ToList();
            }
            catch(Exception e)
            {
                logger.Error("Error in FileParser.readFile() on file " + file, e);
            }

            return result;
        }

        private static List<string> extractMsgDocHead(List<string> lines)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = lines.Find(m => m.StartsWith("MSG"));
                lines.Remove(msg);
                result.Add(msg);

                string doc = lines.Find(m => m.StartsWith("DOC"));
                lines.Remove(doc);
                result.Add(doc);

                string head = lines.Find(m => m.StartsWith("HEAD"));
                result.Add(head);

            }
            catch (Exception e)
            {
                logger.Error("Error in FileParser.extractMsgAndDoc()", e);
            }

            return result;
        }

        private static List<string[]> splitAndsortLines(List<string> lines)
        {
            List<string[]> result = new List<string[]>();

            try
            {
                foreach(string line in lines)
                {
                    string[] splittedLine = line.Split(',');
                    result.Add(splittedLine);
                }

                // Order first by ID then by type (DEL, HEAD, PDN)
                result = result.OrderBy(s=>s[1]).ThenBy(s=>s[0]).ToList();
            }
            catch (Exception e)
            {
                logger.Error("Error in FileParser.splitAndsortLines()", e);
            }

            return result;
        }

        /// <summary>
        /// Takes at splitted and sorted list for extrakting individual plan of each produkt in file
        /// </summary>
        /// <param name="splittedandSortedLines"></param>
        /// <returns></returns>
        private static List<PlanProduct> getPlanProductList(List<string[]> splittedandSortedLines)
        {
            List<PlanProduct> result = new List<PlanProduct>();

            try
            {
                List<string[]> productLineList = new List<string[]>();

                List<List<string[]>> groupOfProductsList = splittedandSortedLines.GroupBy(item => item[1]).Select(group => group.ToList()).ToList();

                foreach (List<string[]> productGroup in groupOfProductsList)
                {
                    PlanProduct pp = getplanProduct(productGroup);
                    result.Add(pp);
                }
            }
            catch(Exception e)
            {
                logger.Error("Error in FileParser.getPlanProductList()", e);
            }

            return result;
        }

        private static PlanProduct getplanProduct(List<string[]> planList)
        {
            PlanProduct result = new PlanProduct();
            
            try
            {
                foreach(string[] line in planList)
                {
                    if(line[0] == "HEAD")
                    {
                        Product product = GarpDb.GetProduct(line[2]);
                        if(product != null)
                        {
                            result.ProductNo = product.ProductNo;
                            result.ProductName = product.Description;
                        }
                        
                        result.ProductNoInFile = line[2];
                        result.BuyerProductNo = line[19];
                        result.Text1 = line[23];
                        result.Text2 = line[14];
                        result.Text3 = line[16];
                        result.Text6 = line[37];

                    }
                    else if (line[0] == "DEL")
                    {
                        PlanDetail pd = getPlanDetail(line);
                        result.PartConsignmentNo = line[9];

                        result.Text4 = line[7];
                        result.Text5 = line[9];

                        if(line.Length > 13)
                        {
                            result.GenericText = line[13];
                        }

                        // Skip plans with zero amount
                        if (pd.Amount > 0)
                        {
                            result.PlanDetail.Add(pd);
                        }
                    }
                    
                }
            }
            catch(Exception e)
            {
                logger.Error("Error in FileParser.getplanProduct()", e);
            }

            return result;
        }

        private static PlanDetail getPlanDetail(string[] line)
        {
            PlanDetail result = new PlanDetail();

            try
            {
                string daysIntoTheFuture = GenericFunctions.GetSettingsValue("HorizionInDays");

                result.DeliverDate = GenericFunctions.getDateFromStr(line[2].Substring(2));
                result.Amount = GenericFunctions.getDecimalFromStr(line[4]);

                // Historical transaction
                if(result.DeliverDate <= DateTime.Now.Date)
                {
                    result.Type = 1;
                }
                // Beoynd our horizon
                else if (result.DeliverDate > DateTime.Now.AddDays(int.Parse(daysIntoTheFuture)))
                {
                    result.Type = 3;
                }
                // Current transaction
                else
                {
                    result.Type = 2;
                }
            }
            catch(Exception e)
            {
                logger.Error("Error in FileParser.getPlanDetail()", e);
            }

            return result;
        }
    }
}
