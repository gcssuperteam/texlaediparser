﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GIS_Dto
{

    public partial class PurchaseInvoice
    {
        [GarpShortFieldName("BOL", 3)]                  			// Bolag
        public string Company { get; set; }
        [GarpShortFieldName("FNR", 6)]                  			// Faktnr/vernr
        public string InvoiceNo { get; set; }
        public string ExternalId { get; set; }                      // Beräknas?
        public string InvoiceType { get; set; }                     // 1 = Debet, 2 = Kredit, hittas inte heller i socket
        public string SupplierName { get; set; }                    // Verkar tilldelas ett värde i socket från en annan tabell
        [GarpShortFieldName("REF", 15)]                  			// Ert faktnr
        public string SupplierInvoiceNo { get; set; }
        public Nullable<decimal> AmountExVat { get; set; }          // Beräknas?
        [GarpShortFieldName("FBL", 13)]                  			// Fakturabelopp
        public Nullable<decimal> AmountDue { get; set; }
        [GarpShortFieldName("TBF", 13)]                  			// Faktuberabel i SEK
        public Nullable<decimal> AmountDueInBaseCurrency { get; set; }
        public Nullable<decimal> RemainingDue { get; set; }         // Beräknas?
        public Nullable<decimal> NextPaymentDue { get; set; }       // Beräknas?
        [GarpShortFieldName("MOM", 1)]                  			// Kod momskonto
        public Nullable<decimal> VAT { get; set; }
        [GarpShortFieldName("VKU", 13)]                  			// Valutakurs
        public Nullable<decimal> Currency { get; set; }
        [GarpShortFieldName("VAL", 3)]                  			// Valutakod
        public string CurrencyCode { get; set; }
        [GarpShortFieldName("SUB", 1)]                  			// Status betalning
        public string PaymentState { get; set; }
        public string PaymentWay { get; set; }                      // Beräknas?
        [GarpShortFieldName("ATS", 4)]                  			// Attest
        public string AttestState { get; set; }
        [GarpShortFieldName("LNR", 10)]                  			// Levnr
        public string SupplierNo { get; set; }
        [GarpShortFieldName("FAD", 6)]                  			// Fakturadatum
        public string InvoiceDate { get; set; }
        [GarpShortFieldName("BVK", 2)]                  			// Betvillkor
        public string PaymentTerms { get; set; }
        public string PaymentPriority { get; set; }                 // Beräknas?
        public string SupplierDebtAccountCode { get; set; }         // Beräknas?
        [GarpShortFieldName("FFD", 6)]                  			// Förfallodatum
        public string PaymentExpiryDate { get; set; }
        public string EDI { get; set; }                             // Beräknas?
        public bool AddDebtAccountFromSupplier { get; set; }        // Beräknas?
        public List<InvoiceAccount> AccountList { get; set; }       // Beräknas?
    }

    public partial class InvoiceAccount
    {
        public string Company { get; set; }
        public string InvoiceNo { get; set; }
        public int RowNo { get; set; }
        public string User { get; set; }
        public string Terminal { get; set; }
        public string AccountPayable { get; set; }
        public string VatCode { get; set; }
        public string Account { get; set; }
        public string SupplierDebtAccountCode { get; set; }
        public string CostPlace { get; set; }
        public string Project { get; set; }
        public string Extra { get; set; }
        public string Text { get; set; }
        public string JournalCode { get; set; }
        public decimal Amount { get; set; }
        public decimal Count { get; set; }
        public decimal AmountInCurrency { get; set; }
        public decimal Currency { get; set; }
        public string JournalNo { get; set; }
        public string VatCode2 { get; set; }
        public string Date { get; set; }
        public string DiscountType { get; set; }
    }

    public partial class CustomerInvoice
    {
        [GarpShortFieldName("BOL", 3)]                  			// Bolag
        public string Company { get; set; }
        [GarpShortFieldName("FNR", 6)]                  			// Fakturanr
        public string InvoiceNo { get; set; }
        [GarpShortFieldName("FTY", 1)]                  			// Fakturatyp
        public string InvoiceType { get; set; }                     // 1 = Debet, 2 = Kredit
        [GarpShortFieldName("HNR", 6)]                  			// Följes(1a)
        public string DeliverNoteNo { get; set; }
        public string CustomerName { get; set; }                    // Verkar tilldelas ett värde från en annan tabell i Socket
        [GarpShortFieldName("FBL", 13)]                  			// Fakturabelopp
        public Nullable<decimal> AmountDue { get; set; }
        [GarpShortFieldName("VBL", 13)]                  			// Varubelopp
        public Nullable<decimal> AmountExVat { get; set; }
        [GarpShortFieldName("ABE", 13)]                  			// Kvar att betala
        public Nullable<decimal> RemainingDue { get; set; }
        [GarpShortFieldName("MOM", 1)]                  			// Kod momskonto
        public Nullable<decimal> VAT { get; set; }
        [GarpShortFieldName("VKU", 13)]                  			// Valutakurs
        public Nullable<decimal> Currency { get; set; }
        [GarpShortFieldName("VAL", 3)]                  			// Valutakod
        public string CurrencyCode { get; set; }
        [GarpShortFieldName("MBL", 13)]                  			// Momsbelopp
        public Nullable<decimal> TotalTax { get; set; }
        [GarpShortFieldName("SUB", 1)]                  			// Status betalning
        public string PaymentState { get; set; }
        //[GarpShortFieldName("ATS", ?)]                  			// Hittar att Socket hämtar från fältet ATS men jag hittar det inte i Garp
        public string AttestState { get; set; }                     // Beräknas eller felaktig?
        [GarpShortFieldName("KNR", 10)]                  			// Kundnr
        public string CustomerNo { get; set; }
        [GarpShortFieldName("FAD", 6)]                  			// Fakturadatum
        public Nullable<System.DateTime> InvoiceDate { get; set; }                      //public string InvoiceDate { get; set; }
        [GarpShortFieldName("BVK", 2)]                  			// Betvillkor
        public string PaymentTerms { get; set; }
        [GarpShortFieldName("FFD", 6)]                              // Förfallodatum
        public Nullable<System.DateTime> PaymentExpiryDate { get; set; }            //public string PaymentExpiryDate { get; set; }
        [GarpShortFieldName("RCT", 1)]                  			// Recordtyp
        public string RecordType { get; set; }
        [GarpShortFieldName("SYS", 1)]                  			// System
        public string System { get; set; }
        [GarpShortFieldName("EDI", 1)]                  			// Edi-flagga
        public string EDI { get; set; }
        public List<InvoiceAccount> AccountList { get; set; }       // Beräknas?
        public string InvoiceDateAsString { get; set; }                         //BP-test
        public string PaymentExpiryDateAsString { get; set; }                         //BP-test
    }

    public partial class CustomerInvoiceMinimalMobile
    {
        public string InvoiceNo { get; set; }
        public string CustomerNo { get; set; }
        public Nullable<decimal> AmountDue { get; set; }
        public string CurrencyCode { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        public Nullable<System.DateTime> PaymentExpiryDate { get; set; }
    }

    //[MetadataType(typeof(Customer.CustomerDTO))] 
    public partial class Customer
    {
        [GarpShortFieldName("KNR", 10)]
        public string CustomerNo { get; set; }
        [GarpShortFieldName("NAM", 30)]
        public string Name { get; set; }
        [GarpShortFieldName("AD1", 30)]
        public string Street { get; set; }
        [GarpShortFieldName("AD2", 30)]
        public string Box { get; set; }
        //[GarpShortFieldName("AD3", 30)]
        public string Address3 { get; set; }
        [GarpShortFieldName("LND", 2)]
        public string CountryId { get; set; }
        [GarpShortFieldName("FKN", 10)]
        public string InvoiceCustomerNo { get; set; }
        //[GarpShortFieldName("", 1)]
        public string InvoiceCustomerName { get; set; }
        [GarpShortFieldName("SMF", 1)]
        public string UnifiedInvoice { get; set; }
        [GarpShortFieldName("ORT", 30)]
        public string ZipCity { get; set; }
        [GarpShortFieldName("BVK", 2)]
        public string PaymentTermsId { get; set; }
        [GarpShortFieldName("LVK", 2)]
        public string DeliverTermsId { get; set; }
        [GarpShortFieldName("LSE", 2)]
        public string DeliverWay { get; set; }
        [GarpShortFieldName("LSP", 1)]
        public string DeliverLockId { get; set; }
        //[GarpShortFieldName("", 1)]
        public string DeliverLockDescription { get; set; }
        [GarpShortFieldName("SPR", 1)]
        public string LanguageCode1 { get; set; }
        [GarpShortFieldName("SP2", 1)]
        public string LanguageCode2 { get; set; }
        [GarpShortFieldName("SLJ", 2)]
        public string SellerId { get; set; }
        [GarpShortFieldName("VAL", 3)]
        public string CurrencyId { get; set; }
        [GarpShortFieldName("KAT", 2)]
        public string CategoryId { get; set; }
        //[GarpShortFieldName("", 1)]
        public string CategoryDescription { get; set; }
        [GarpShortFieldName("KTO", 2)]
        public string AccountId { get; set; }
        [GarpShortFieldName("KTY", 1)]
        public string CustomerType { get; set; }
        [GarpShortFieldName("TEL", 18)]
        public string Phone { get; set; }
        [GarpShortFieldName("FAX", 18)]
        public string Fax { get; set; }
        [GarpShortFieldName("REF", 20)]
        public string Reference { get; set; }
        [GarpShortFieldName("ORG", 10)]
        public string CorporateIdentityNo { get; set; }
        //[GarpShortFieldName("", 1)]
        public string Text { get; set; }
        [GarpShortFieldName("TX1", 20)]
        public string Text_1 { get; set; }
        [GarpShortFieldName("TX2", 20)]
        public string Text_2 { get; set; }
        [GarpShortFieldName("TX3", 20)]
        public string Text_3 { get; set; }
        [GarpShortFieldName("TX4", 20)]
        public string Text_4 { get; set; }
        [GarpShortFieldName("TX5", 20)]
        public string Text_5 { get; set; }
        [GarpShortFieldName("TX6", 20)]
        public string Text_6 { get; set; }
        [GarpShortFieldName("TX7", 20)]
        public string Text_7 { get; set; }
        [GarpShortFieldName("TX8", 20)]
        public string Text_8 { get; set; }
        [GarpShortFieldName("TX9", 20)]
        public string Text_9 { get; set; }
        [GarpShortFieldName("TX10", 20)]
        public string Text_10 { get; set; }
        //[GarpShortFieldName("", 1)]
        public string DeliverZip { get; set; }
        [GarpShortFieldName("RBK", 1)]
        public string DiscountId { get; set; }
        [GarpShortFieldName("PRL", 1)]
        public string PriceListId { get; set; }
        [GarpShortFieldName("RDB", 1)]
        public string InterestInvoice { get; set; }
        [GarpShortFieldName("KRV", 1)]
        public string ClaimCode { get; set; }
        [GarpShortFieldName("OIM", 1)]
        public string DutyFree { get; set; }
        [GarpShortFieldName("SER", 6)]
        public string LastInvoiceDate { get; set; }
        [GarpShortFieldName("MOM", 1)]
        public string VATId { get; set; }
        [GarpShortFieldName("LIM", 9)]
        public Nullable<decimal> CreditLimit { get; set; }
        [GarpShortFieldName("KVD", 7)]
        public Nullable<decimal> CalculatedCreditTime { get; set; }
        [GarpShortFieldName("SLD", 15)]
        public Nullable<decimal> OpenAmount { get; set; }
        [GarpShortFieldName("ODA", 15)]
        public Nullable<decimal> TurnoverThisYear { get; set; }
        [GarpShortFieldName("OFA", 15)]
        public Nullable<decimal> TurnoverLastYear { get; set; }
        [GarpShortFieldName("TDA", 15)]
        public Nullable<decimal> ContributionMarginThisYear { get; set; }
        [GarpShortFieldName("TFA", 15)]
        public Nullable<decimal> ContributionMarginLastYear { get; set; }
        [GarpShortFieldName("TPT", 9)]
        public string TransportTime { get; set; }
        [GarpShortFieldName("NM1", 11)]
        public Nullable<decimal> Numeric_1 { get; set; }
        [GarpShortFieldName("NM2", 11)]
        public Nullable<decimal> Numeric_2 { get; set; }
        [GarpShortFieldName("NM3", 11)]
        public Nullable<decimal> Numeric_3 { get; set; }
        [GarpShortFieldName("NM4", 11)]
        public Nullable<decimal> Numeric_4 { get; set; }
        [GarpShortFieldName("KD1", 1)]
        public string Code_1 { get; set; }
        [GarpShortFieldName("KD2", 1)]
        public string Code_2 { get; set; }
        [GarpShortFieldName("KD3", 1)]
        public string Code_3 { get; set; }
        [GarpShortFieldName("KD4", 1)]
        public string Code_4 { get; set; }
        [GarpShortFieldName("KD5", 1)]
        public string Code_5 { get; set; }
        [GarpShortFieldName("KD6", 1)]
        public string Code_6 { get; set; }
        [GarpShortFieldName("GPN", 6)]
        public string GoodsZip { get; set; }
        [GarpShortFieldName("PO1", 1)]
        public string PriceListOrderType1 { get; set; }
        [GarpShortFieldName("PO2", 1)]
        public string PriceListOrderType2 { get; set; }
        [GarpShortFieldName("PP1", 1)]
        public string PriceList1 { get; set; }
        [GarpShortFieldName("PP2", 1)]
        public string PriceList2 { get; set; }
        //public string EDIRecipientadress { get; set; }
        public string EDIRecipientAddress { get; set; }
        public string EANRecipientAddress { get; set; }
        public string EANSenderAddress { get; set; }
        public string SellerName { get; set; }
        public string CountryName { get; set; }
        public string CategoryName { get; set; }
    }

    public partial class CustomerMinimalMobile
    {
        public string CustomerNo { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string ZipCity { get; set; }
        public string SortField { get; set; }
        public string Phone { get; set; }
    }

    public partial class CustomerMinimal
    {
        public string CustomerNo { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string Box { get; set; }
        public string ZipCity { get; set; }
        public string CategoryId { get; set; }
        public string CategoryDescription { get; set; }
        public string CustomerType { get; set; }
        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public string DeliverLockId { get; set; }
        public string InvoiceCustomerNo { get; set; }
        public string InvoiceCustomerName { get; set; }
        public string SellerId { get; set; }
        public string SellerName { get; set; }
        public Nullable<decimal> OpenAmount { get; set; }

        public List<FieldAnValues> CustomFieldList { get; set; }
    }


    public partial class CustomerMobile
    {
        public string CustomerNo { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string Box { get; set; }
        public string CountryId { get; set; }
        public string ZipCity { get; set; }
        public string Reference { get; set; }
        public string PaymentTerms { get; set; }
        public string DeliverTerms { get; set; }
        public string DeliverWay { get; set; }
        public string Seller { get; set; }
        public string CategoryId { get; set; }
        public string Phone { get; set; }
        public string CorporateIdentityNo { get; set; }
        public DateTime? LastInvoiceDate { get; set; }
        public Nullable<decimal> CreditLimit { get; set; }
        public Nullable<decimal> CalculatedCreditTime { get; set; }
        public Nullable<decimal> OpenAmount { get; set; }
        public Nullable<decimal> TurnoverThisYear { get; set; }
        public Nullable<decimal> TurnoverLastYear { get; set; }
        public Nullable<decimal> ContributionMarginThisYear { get; set; }
        public Nullable<decimal> ContributionMarginLastYear { get; set; }

        List<FieldAnValues> CustomFieldList { get; set; }
    }

    public class CustomerStatistics
    {
        public string Id { get; set; }
        public decimal OpenOrderValue { get; set; }
        public decimal OpenOfferValue { get; set; }
        public decimal OpenInvoiceValue { get; set; }
        public int OpenOrderCount { get; set; }
        public int OpenOfferCount { get; set; }
        public DateTime LastInvoicDate { get; set; }
        public List<OrderHeadMinimal> OrderList { get; set; }
        public List<OrderHeadMinimal> OfferList { get; set; }
        public List<CustomerInvoiceMinimalMobile> InvoiceList { get; set; }
        public List<CustomerInvoiceMinimalMobile> OverdueInvoiceList { get; set; }
        public List<CustomerInvoiceMinimalMobile> OverdueMoreThan15DaysInvoiceList { get; set; }
    }

    public partial class PurchaseOrderHead
    {
        [GarpShortFieldName("ONR", 6)]                  			// Ordernummer
        public string OrderNo { get; set; }
        [GarpShortFieldName("ODT", 6)]                  			// Orderdatum
        public string Date { get; set; }
        public string DeliverAddress1 { get; set; }                 // Denna rad ner till CountryCode finns inte i PurchaseOrderSocket, antar att värdena hämtas från en annan tabell
        public string DeliverAddress2 { get; set; }
        public string DeliverZipCity { get; set; }
        public string CountryCode { get; set; }
        [GarpShortFieldName("OTY", 1)]                  			// Ordertyp
        public string OrderType { get; set; }
        [GarpShortFieldName("ERF", 20)]                  			// Er referens
        public string YoureReference { get; set; }
        [GarpShortFieldName("VRF", 2)]                  			// Vår referens
        public string OurReferenceId { get; set; }
        public string OurReferenceName { get; set; }                // Finns inte i PurchaseOrderSocket. Antar att den hämtas med hjälp av värdet i föregående fält.
        public string SellerId { get; set; }                        // Finns inte i PurchaseOrderSocket
        public string SellerName { get; set; }                      // Finns inte i PurchaseOrderSocket
        [GarpShortFieldName("LVK", 2)]                  			// Leveransvillkor
        public string DeliverTerms { get; set; }
        [GarpShortFieldName("LSE", 2)]                  			// Levsätt
        public string DeliverWay { get; set; }
        public string DeliverWayText { get; set; }                  // Finns inte i PurchaseOrderSocket. Hämtas väl mha föregående värde.
        public string CalculatedDeliverDate { get; set; }           // Finns inte. Beräknas?
        public string InvoiceAddress1 { get; set; }                 // Finns inte. Hämtas från annan tabell?
        public string InvoiceAddress2 { get; set; }                 // Finns inte. Hämtas från annan tabell?
        public string InvoiceZipCity { get; set; }                  // Finns inte. Hämtas från annan tabell?
        [GarpShortFieldName("LEF", 1)]                  			// Lev-fl
        public string DeliverState { get; set; }
        [GarpShortFieldName("BVK", 2)]                  			// Betvillk
        public string PaymentTerms { get; set; }
        [GarpShortFieldName("BLT", 6)]                  			// Bekr/önskad lev tid
        public string PreferedDeliverDate { get; set; }
        [GarpShortFieldName("VAL", 3)]                  			// Valutakod
        public string CurrencyId { get; set; }
        [GarpShortFieldName("NX2", 9)]                  			// OrderKurs
        public string CurrencyValue { get; set; }
        [GarpShortFieldName("KTO", 2)]                  			// Kontokod, KOLLA ACCOUNTID OCH ACCOUNTNO
        public string AccountId { get; set; }
        [GarpShortFieldName("MOM", 1)]                  			// Momskod
        public string VatId { get; set; }
        [GarpShortFieldName("SES", 3)]                  			// Säsong
        public string Season { get; set; }
        [GarpShortFieldName("LNR", 10)]                  			// Leverantör
        public string SupplierNo { get; set; }
        public string SupplierName { get; set; }                    // Finns inte. Annan tabell?
        public string InvoiceCustomerNo { get; set; }               // Finns inte. Annan tabell?
        public string OrderConfirmationState { get; set; }          // Finns inte. Beräknas?
        [GarpShortFieldName("GMF", 1)]                  			// GM-fl
        public string GoodsReceiptState { get; set; }
        [GarpShortFieldName("X1F", 1)]                  			// X1-fl
        public string X1F { get; set; }
        [GarpShortFieldName("LDT", 6)]                  			// Sen leveransdatum
        public string LastDeliverDate { get; set; }
        [GarpShortFieldName("PRL", 1)]                  			// Prislista
        public string PriceListId { get; set; }
        [GarpShortFieldName("BOL", 3)]                  			// Bolag i redov
        public string CompanyId { get; set; }
        [GarpShortFieldName("FRY", 1)]                  			// z Fryzt
        public string Stopped { get; set; }
        [GarpShortFieldName("RES", 1)]                  			// Reservationsstatus
        public string ReservedId { get; set; }
        [GarpShortFieldName("OLV", 13)]                  			// Olevererat ordervärde
        public string NotDeliveredValue { get; set; }
        [GarpShortFieldName("LVV", 13)]                  			// Levererat värde
        public string DeliveredValue { get; set; }
        [GarpShortFieldName("TPT", 9)]                  			// Transporttid
        public string TransportTime { get; set; }
        public string DeliverPath { get; set; }                     // Finns inte.
        public string DeliverPlace { get; set; }                    // Finns inte.
        [GarpShortFieldName("OKL", 4)]                  			// Order_ttmm
        public string Order_ttmm { get; set; }
        [GarpShortFieldName("KTO", 2)]                  			// Kontokod. Hämtar värde från samma kortkod som AccountId. Kolla upp!
        public string AccountNo { get; set; }
        [GarpShortFieldName("KST", 4)]                  			// Kostnadsställe
        public string CostPlaceId { get; set; }
        [GarpShortFieldName("PRO", 6)]                  			// Projekt
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }                     // Finns inte. Hämtas nog mha ovan värde.
        [GarpShortFieldName("EXT", 2)]                  			// Extra
        public string ExtraId { get; set; }
        [GarpShortFieldName("RBK", 1)]                  			// Generell rabattkod
        public string DiscountId { get; set; }
        [GarpShortFieldName("RAB", 7)]                  			// Orderrabatt proc - minus
        public string DiscountPercentage { get; set; }
        public string LastDeliverNote { get; set; }                 // Finns inte, alla fram tom Text4
        public string LastDeliverNoteState { get; set; }
        public string ShipmentNo { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal TotalVolyme { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string Text3 { get; set; }
        public string Text4 { get; set; }
        [GarpShortFieldName("INK", 2)]                  			// Inköpare
        public string Purchaser { get; set; }
        [GarpShortFieldName("IOF", 1)]                  			// IO-fl
        public string PurchaseState { get; set; }
        public bool ConnectedOrder { get; set; }                    // Finns inte.

        public SellerDTO Seller { get; set; }                       // Finns inte.
        public List<OrderRow> OrderRows { get; set; }               // Finns inte.
    }

    public partial class OrderHead
    {
        [GarpShortFieldName("ONR", 6)]                      // Ordernummer
        public string OrderNo { get; set; }
        [GarpShortFieldName("ODT", 6)]                      // Orderdatum
        public string Date { get; set; }
        public string DeliverAddress1 { get; set; }         // Fylls i från annan tabell, ner till CountryCode
        public string DeliverAddress2 { get; set; }
        public string DeliverAddress3 { get; set; }
        public string DeliverZipCity { get; set; }
        public string DeliverZip { get; set; }
        public string DeliverCity { get; set; }
        public string CountryCode { get; set; }
        [GarpShortFieldName("OTY", 1)]                      // Ordertyp
        public string OrderType { get; set; }
        [GarpShortFieldName("ERF", 20)]                     // Er referens
        public string YoureReference { get; set; }
        [GarpShortFieldName("VRF", 2)]                      // Vår referens
        public string OurReferenceId { get; set; }
        public string OurReferenceName { get; set; }        // Fylls i från annan tabell
        [GarpShortFieldName("SLJ", 2)]                      // Säljare/distr
        public string SellerId { get; set; }
        public string SellerName { get; set; }              // Fylls i från annan tabell
        [GarpShortFieldName("LVK", 2)]                      // Levvillk
        public string DeliverTerms { get; set; }
        public string DeliverTermsDescription { get; set; } // Fylls i från annan tabell
        [GarpShortFieldName("LSE", 2)]                      // Levsätt
        public string DeliverWay { get; set; }
        public string DeliverWayDescription { get; set; }   // Fylls i från annan tabell
        [GarpShortFieldName("LEF", 1)]                      // Lev-fl
        public string DeliverState { get; set; }
        [GarpShortFieldName("BVK", 2)]                      // Betvillkor
        public string PaymentTerms { get; set; }
        [GarpShortFieldName("BLT", 6)]                      // Bekr/önskad lev tid
        public string PreferedDeliverDate { get; set; }
        [GarpShortFieldName("VAL", 3)]                      // Valutakod
        public string CurrencyId { get; set; }
        [GarpShortFieldName("NX2", 9)]                      // Orderkurs
        public string CurrencyValue { get; set; }
        [GarpShortFieldName("MOM", 1)]                      // Momskod
        public string VatId { get; set; }
        [GarpShortFieldName("SES", 3)]                      // Säsong
        public string Season { get; set; }
        [GarpShortFieldName("KNR", 10)]                     // Orderkund
        public string DeliverCustomerNo { get; set; }       // Fylls i från annan tabell, ner till DeliverCustomerType
        public string DeliverCustomerName { get; set; }
        public string DeliverCustomerPhone { get; set; }
        public string DeliverCustomerCode { get; set; }
        public string DeliverCustomerLockId { get; set; }
        public string DeliverCustomerType { get; set; }
        [GarpShortFieldName("FKN", 10)]                     // Fakturakund
        public string InvoiceCustomerNo { get; set; }
        [GarpShortFieldName("OBF", 1)]                  	// OB-fl
        public string OrderConfirmationState { get; set; }
        [GarpShortFieldName("PLF", 1)]                  	// PL-fl
        public string PickListState { get; set; }
        [GarpShortFieldName("X1F", 1)]                  	// X1-fl
        public string X1F { get; set; }
        [GarpShortFieldName("LDT", 6)]                  	// Sen leveransdatum
        public string LastDeliverDate { get; set; }
        [GarpShortFieldName("PRL", 1)]                  	// Prislista
        public string PriceListId { get; set; }
        [GarpShortFieldName("BOL", 3)]                  	// Bolag i redov
        public string CompanyId { get; set; }
        [GarpShortFieldName("FRY", 1)]                  	// z Fryzt
        public string Stopped { get; set; }
        [GarpShortFieldName("RES", 1)]                  	// Reservationsstatus
        public string ReservedId { get; set; }
        [GarpShortFieldName("OLV", 13)]                  	// Olevererat ordervärde
        public string NotDeliveredValue { get; set; }
        [GarpShortFieldName("LVV", 13)]                  	// Levererat värde
        public string DeliveredValue { get; set; }
        [GarpShortFieldName("TPT", 9)]                  	// Transporttid
        public string TransportTime { get; set; }
        [GarpShortFieldName("LTU", 8)]                  	// Leveranstur
        public string DeliverPath { get; set; }
        [GarpShortFieldName("LTP", 2)]                  	// Leveransplac
        public string DeliverPlace { get; set; }
        [GarpShortFieldName("OKL", 4)]                  	// Order_ttmm
        public string Order_ttmm { get; set; }
        [GarpShortFieldName("KTO", 2)]                  	// Kontokod
        public string AccountNo { get; set; }
        [GarpShortFieldName("KST", 4)]                  	// Kostnadsställe
        public string CostPlaceId { get; set; }
        [GarpShortFieldName("PRO", 6)]                  	// Projekt
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }             // Fylls i från annan tabell
        [GarpShortFieldName("EXT", 2)]                  	// Extra
        public string ExtraId { get; set; }
        [GarpShortFieldName("RBK", 1)]                  	// Generell rabattkod
        public string DiscountId { get; set; }
        [GarpShortFieldName("RAB", 7)]                  	// Orderrabatt proc - ...
        public string DiscountPercentage { get; set; }
        public string LastDeliverNote { get; set; }         // Samtliga värden nedan utom 2, NOR och FOR, beräknas eller hämtas från andra tabeller
        public string LastDeliverNoteState { get; set; }
        public string ShipmentNo { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal TotalVolyme { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string Text3 { get; set; }
        public string Text4 { get; set; }
        [GarpShortFieldName("NOR", 6)]                  	// Forts_på_ordernr
        public string ContinueOnOrder { get; set; }
        [GarpShortFieldName("FOR", 6)]                  	// Forts_fr_ordernr
        public string ContinueFromOrder { get; set; }
        public bool AdditionalOrders { get; set; }
        public string[] CustomerInfo { get; set; }
        public bool? IsDeliveryAddressUnique { get; set; }
        public Customer DeliverCustomer { get; set; }
        public SellerDTO Seller { get; set; }
        public List<OrderRow> OrderRows { get; set; }
        public List<OrderRowText> OrderRowZeroTexts { get; set; }
        public string InvoiceCustomerName { get; set; }
        public string InvoiceCustomerAddress1 { get; set; }
        public string InvoiceCustomerAddress2 { get; set; }
        public string InvoiceCustomerAddress3 { get; set; }
        public string InvoiceCustomerZipCode { get; set; }
        public string InvoiceCustomerCity { get; set; }
        public string InvoiceCustomerCountryCode { get; set; }
        public string InvoiceCustomerVATId { get; set; }
        public string ShippingNotification { get; set; }
        public string ShippingNotificationType { get; set; }
        public string DeliverInstruction { get; set; }

    }


    public partial class InvoicePdf
    {
        public string InvoiceNo { get; set; }
        public string PdfBase64String { get; set; }
    }

    public partial class MaterialOrderHead
    {
        public string OrderNo { get; set; }
        public string RowNo { get; set; }
        public string Date { get; set; }
        public string OrderType { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }
        public decimal Amount { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string CompanyId { get; set; }
        public string DeliverState { get; set; }
        public string ProdPlanState { get; set; }
        public string MaterialPlanState { get; set; } // MTF
        public string DeliverableState { get; set; }  // BLF
        public List<MaterialOrderRow> MaterialOrderRows { get; set; }
        public List<MaterialRow> MaterialRows { get; set; }
    }

    public partial class ProductionOrderHead
    {
        public string OrderNo { get; set; }
        public string RowNo { get; set; }
        public string Date { get; set; }
        public string OrderType { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }
        public decimal Amount { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string CompanyId { get; set; }
        public string DeliverState { get; set; }
        public string ProdPlanState { get; set; }
        public string MaterialPlanState { get; set; } // MTF
        public string DeliverableState { get; set; }  // BLF
        public string Unit { get; set; }  // BLF
        public List<OperationRow> OperationRows { get; set; }
        public List<MaterialRow> MaterialRows { get; set; }
    }

    public partial class MaterialOrderMinimal
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }
        public decimal Amount { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string ProduktionGroup { get; set; }
        public string StartTime { get; set; }
        public List<MaterialRowMinimal> MaterialRows { get; set; }
    }

    public partial class MaterialOrderRow
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }
        public string ProductDescription2 { get; set; }
        public decimal Amount { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string MaterialPlanFlag { get; set; }
        public string MaterialDocumentFlag { get; set; }
        public bool FullyDeliverable { get; set; }
        public Product Product { get; set; }
        public List<MaterialRow> MaterialRows { get; set; }
        public List<OperationRow> OperationRows { get; set; }

    }

    public partial class OrderHeadMinimal
    {
        public string OrderNo { get; set; }
        public string OrderType { get; set; }
        public string Date { get; set; }
        public string PreferedDeliverDate { get; set; }
        public decimal OpenValue { get; set; }
        public string DeliverCustomerNo { get; set; }
        public string DeliverCustomerName { get; set; }
    }

    public partial class OrderMobile
    {
        public string OrderNo { get; set; }
        public string Date { get; set; }
        public string PreferedDeliverDate { get; set; }
        public decimal OpenValue { get; set; }
        List<FieldAnValues> CustomFieldList { get; set; }
        List<OrderRow> OrderRowList { get; set; }
    }

    public class OrderHeadDeliverAddress
    {
        string mOSE = "K", mRCT = "A";
        public string OrderId { get; set; }
        public string OrderSerie { get { return mOSE; } set { } }
        public string RecordType { get { return mRCT; } set { } }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string ZipCity { get; set; }
        public string Country { get; set; }
    }

    public partial class OrderRow
    {
        //string mOSE = "K", mRCT = "I";

        [GarpShortFieldName("ONR", 6)]                  	// Ordernummer
        public string OrderNo { get; set; }
        [GarpShortFieldName("RDC", 3)]                  	// Radnummer
        public int RowNo { get; set; }
        [GarpShortFieldName("ANR", 13)]                  	// Artikelnr
        public string ProductNo { get; set; }
        [GarpShortFieldName("LAG", 7)]                  	// Lagernr
        public string WarehouseNo { get; set; }
        [GarpShortFieldName("BEN", 20)]                  	// Benämning
        public string ProductDescription { get; set; }
        [GarpShortFieldName("BNX", 20)]                  	// Förlängd benämning
        public string ProductDescription2 { get; set; }
        [GarpShortFieldName("KTO", 6)]                  	// Kontokod
        public string AccountId { get; set; }
        [GarpShortFieldName("KST", 4)]                  	// Kostnadsställe
        public string CostPlaceId { get; set; }
        [GarpShortFieldName("PRO", 6)]                  	// Projekt
        public string ProjectId { get; set; }
        [GarpShortFieldName("EXT", 2)]                  	// Extra
        public string ExtraId { get; set; }
        [GarpShortFieldName("ENH", 3)]                  	// Enhet
        public string Unit { get; set; }
        [GarpShortFieldName("LDT", 6)]                  	// Leveranstid
        public string PreferedDeliverDate { get; set; }
        [GarpShortFieldName("MTF", 1)]                  	// M-fl
        public string MTF { get; set; }
        [GarpShortFieldName("BLF", 1)]                  	// B-fl
        public string BLF { get; set; }
        [GarpShortFieldName("MDF", 1)]                  	// MD-fl
        public string MDF { get; set; }
        [GarpShortFieldName("BDF", 1)]                  	// BD-fl
        public string BDF { get; set; }
        [GarpShortFieldName("X1F", 1)]                  	// Flagga1
        public string X1F { get; set; }
        [GarpShortFieldName("X2F", 1)]                  	// Flagga2
        public string X2F { get; set; }
        [GarpShortFieldName("NX1", 11)]                  	// NX1
        public decimal? NX1 { get; set; }
        [GarpShortFieldName("DIM", 11)]                  	// Extra antal
        public decimal? DIM { get; set; }
        [GarpShortFieldName("MKT", 2)]                  	// Motkonto
        public string MKT { get; set; }
        [GarpShortFieldName("LVF", 1)]                  	// Lev-fl
        public string DeliverState { get; set; }
        [GarpShortFieldName("INK", 1)]                  	// IK-fl
        public string PurchaseState { get; set; }
        [GarpShortFieldName("OBF", 1)]                  	// OB-fl
        public string OrderConfirmationState { get; set; }
        [GarpShortFieldName("PLF", 1)]                  	// PL-fl
        public string PickListState { get; set; }
        [GarpShortFieldName("LPF", 1)]                  	// Fixerad varukostn
        public string FixedCostPrice { get; set; }
        [GarpShortFieldName("RAT", 1)]                  	// Radtyp
        public string RowType { get; set; }
        [GarpShortFieldName("BRA", 1)]                  	// Beloppsrad flagga
        public string AmountState { get; set; }
        [GarpShortFieldName("HRF", 1)]                  	// OH-rabatt J/N
        public bool UseOHDiscount { get; set; }
        [GarpShortFieldName("RBK", 2)]                  	// Rabattkod
        public string DiscountId { get; set; }
        [GarpShortFieldName("RAB", 7)]                  	// Orderrabatt
        public string DiscountPercentage { get; set; }
        [GarpShortFieldName("USP", 2)]                  	// Ursprung
        public string Origin { get; set; }
        [GarpShortFieldName("MOM", 1)]                  	// Momskod
        public string VatId { get; set; }
        [GarpShortFieldName("RES", 1)]                  	// Reservationsstatus
        public string ReservedId { get; set; }
        [GarpShortFieldName("ORA", 11)]                  	// Ursprungligt antal
        public decimal? Amount { get; set; }
        [GarpShortFieldName("PRI", 11)]                  	// Förs pris
        public decimal? Price { get; set; }
        [GarpShortFieldName("TLA", 11)]                  	// Levererat antal
        public decimal? DeliveredAmount { get; set; }
        [GarpShortFieldName("UKA", 11)]                  	// Kasserat antal
        public decimal? DiscardAmount { get; set; }
        [GarpShortFieldName("PRU", 11)]                  	// Bruttopris
        public decimal? GrossPrice { get; set; }
        [GarpShortFieldName("LVP", 11)]                  	// Varukostnad
        public decimal? CostPrice { get; set; }
        [GarpShortFieldName("STF", 1)]                  	// Statistikflagga
        public string StatisticState { get; set; }
        [GarpShortFieldName("PDE", 3)]                  	// Prisdecimaler
        public int? PriceDecimalCount { get; set; }
        [GarpShortFieldName("ADE", 3)]                  	// Antalsdecimaler
        public int? AmountDecimalCount { get; set; }
        [GarpShortFieldName("BRA", 1)]                  	// Beloppsrad flagga DUBBEL!!!
        public string BRA { get; set; }
        [GarpShortFieldName("HRF", 1)]                  	// OH-rabatt J/N DUBBEL!!!
        public string HRF { get; set; }
        [GarpShortFieldName("KTR", 1)]                  	// Relationskopplad
        public string KTR { get; set; }
        public decimal? AmountToDeliver { get; set; }

        public Product Product { get; set; }
        public List<OrderRowText> Textrows { get; set; }

        public decimal RowValue { get; set; }
        public decimal NetPrice { get; set; }
    }

    public partial class OrderRowText
    {
        //string mOSE = "K", mRCT = "K";
        public string OrderNo { get; set; }
        public string RowNo { get; set; }
        public string TextRowNo { get; set; }
        public string OrderConfirmationState { get; set; }
        public string PickListState { get; set; }
        public string DeliverNoteState { get; set; }
        public string InvoiceState { get; set; }
        public string Text { get; set; }

    }

    public partial class DeliverNoteRowText
    {
        //string mOSE = "K", mRCT = "K";
        public string DeliverNoteNo { get; set; }
        public string RowNo { get; set; }
        public string InomNo { get; set; }
        public string TextRowNo { get; set; }
        public string OrderConfirmationState { get; set; }
        public string PickListState { get; set; }
        public string DeliverNoteState { get; set; }
        public string InvoiceState { get; set; }
        public string Text { get; set; }

    }

    public class Extra
    {
        public string Id { get; set; }

        public string Company { get; set; }


        public string Name { get; set; }
        public string Remark { get; set; }
        public string Type { get; set; }

        public string Responsible { get; set; }

        public string LockCode { get; set; }
    }


    public class CostPlace
    {
        public string Id { get; set; }

        public string Company { get; set; }


        public string Name { get; set; }
        public string Remark { get; set; }
        public string Type { get; set; }

        public string Responsible { get; set; }

        public string LockCode { get; set; }
    }

    public class Project
    {
        public string Id { get; set; }
        public string Company { get; set; }

        public string Type { get; set; }

        public string LockCode { get; set; }

        public string Name { get; set; }

        public string Remark { get; set; }

        public string Responsible { get; set; }
    }

    public partial class User
    {
        public int Id { get; set; }
        public string GarpServerAddress { get; set; }
        public string GarpServerPort { get; set; }
        public string MainDbDir { get; set; }
        public string UserName { get; set; }
        public string UserRoleName { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string CurrentSite { get; set; }
        public string CurrentCompany { get; set; }
        public string RoleId { get; set; }


    }

    public partial class UserErpInfo
    {
        public string Company { get; set; }
        public string CompanyName { get; set; }
        public string Box { get; set; }
        public string Street { get; set; }
        public string ZipCity { get; set; }
        public string VAT { get; set; }
        public string OrgNo { get; set; }
        public string CountryCode { get; set; }
        public string IBAN { get; set; }
        public string BIC { get; set; }
        public string BankPartyId { get; set; }
        //public Currency CompanyCurrency { get; set; }
        //public ErrandSetting ErrandSettings { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string WebAddress { get; set; }
        public string BankGiro { get; set; }
        public string PlusGiro { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string Text3 { get; set; }
        public string Text4 { get; set; }
        public string Text5 { get; set; }
        public string Text6 { get; set; }
        public string Text7 { get; set; }
        public string BankOrCheckAccountNo { get; set; }
    }

    public class OurReferenceDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
    }

    public class SellerDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ExtName { get; set; }
        public string CostPlace { get; set; }
        public string UserId { get; set; }
        public string EmployeNo { get; set; }
    }

    public partial class Contact
    {
        private string mName;
        private string mPhoneDirect;
        private string mMail;
        private string mPhoneMobile;
        private string mPhonePrivate;
        private string mDepartment;
        private string mAddress1;
        private string mAddress2;
        private string mAddress3;
        private string mBelongsToCompanyId;
        private string mContactId;
        private string mRole;
        private string mTelefoneExtra1;
        private string mTelefoneExtra2;
        private string mWebb;
        private string mInformation;
        private string mLoginUser;
        private string mLoginPassword;
        private string mSortiment;

        public string Id { get; set; }
        public bool MailOrder { get; set; }
        public bool MailDeliver { get; set; }
        public bool MailInvoice { get; set; }
        public bool MailReminder { get; set; }
        public int Index { get; set; }
        public List<string> Activities { get; set; }

        //        public int Id { get; set; }
        public string Name
        {
            get
            {
                return mName == null ? "" : mName.PadRight(20).Substring(0, 20).Trim();
            }
            set
            {
                mName = value;
            }
        }
        public string PhoneDirect
        {
            get
            {
                return mPhoneDirect == null ? "" : mPhoneDirect.PadRight(12).Substring(0, 12).Trim();
            }
            set
            {
                mPhoneDirect = value;
            }
        }
        public string Mail
        {
            get
            {
                return mMail == null ? "" : mMail.PadRight(50).Substring(0, 50).Trim();
            }
            set
            {
                mMail = value;
            }
        }

        public string PhoneMobile
        {
            get
            {
                return mPhoneMobile == null ? "" : mPhoneMobile.PadRight(12).Substring(0, 12).Trim();
            }
            set
            {
                mPhoneMobile = value;
            }
        }

        public string PhonePrivate
        {
            get
            {
                return mPhonePrivate == null ? "" : mPhonePrivate.PadRight(12).Substring(0, 12).Trim();
            }
            set
            {
                mPhonePrivate = value;
            }
        }

        public string Department
        {
            get
            {
                return mDepartment == null ? "" : mDepartment.PadRight(20).Substring(0, 20).Trim();
            }
            set
            {
                mDepartment = value;
            }
        }

        public string Address1
        {
            get
            {
                return mAddress1 == null ? "" : mAddress1.PadRight(40).Substring(0, 40).Trim();
            }
            set
            {
                mAddress1 = value;
            }
        }

        public string Address2
        {
            get
            {
                return mAddress2 == null ? "" : mAddress2.PadRight(40).Substring(0, 40).Trim(); ;
            }
            set
            {
                mAddress2 = value;
            }
        }

        public string Address3
        {
            get
            {
                return mAddress3 == null ? "" : mAddress3.PadRight(40).Substring(0, 40).Trim(); ;
            }
            set
            {
                mAddress3 = value;
            }
        }

        public string BelongsToCompanyId
        {
            get
            {
                return mBelongsToCompanyId.Trim();
            }
            set
            {
                mBelongsToCompanyId = value;
            }
        }

        public string ContactId
        {
            get
            {
                return mContactId == null ? "" : mContactId.PadRight(4).Substring(0, 4).Trim(); ;
            }
            set
            {
                mContactId = value;
            }
        }

        public string Role
        {
            get
            {
                return mRole == null ? "" : mRole.PadRight(3).Substring(0, 3).Trim();
            }
            set
            {
                mRole = value;
            }
        }

        public string RoleDescription { get; set; }

        public string TelefoneExtra1
        {
            get
            {
                return mTelefoneExtra1 == null ? "" : mTelefoneExtra1.PadRight(20).Substring(0, 20).Trim(); ;
            }
            set
            {
                mTelefoneExtra1 = value;
            }
        }

        public string TelefoneExtra2
        {
            get
            {
                return mTelefoneExtra2 == null ? "" : mTelefoneExtra2.PadRight(20).Substring(0, 20).Trim(); ;
            }
            set
            {
                mTelefoneExtra2 = value;
            }
        }

        public string Webb
        {
            get
            {
                return mWebb == null ? "" : mWebb.PadRight(50).Substring(0, 50).Trim(); ;
            }
            set
            {
                mWebb = value;
            }
        }

        public string Information
        {
            get
            {
                return mInformation;
            }
            set
            {
                mInformation = value;
            }
        }

        public string LoginUser
        {
            get
            {
                return mLoginUser;
            }
            set
            {
                mLoginUser = value;
            }
        }

        public string LoginPassword
        {
            get
            {
                return mLoginPassword;
            }
            set
            {
                mLoginPassword = value;
            }
        }

        public string Sortiment
        {
            get
            {
                return mSortiment;
            }
            set
            {
                mSortiment = value;
            }
        }
    }

    public partial class Errand
    {
        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ContactPerson { get; set; }
        public string ActivityId { get; set; }
        public string ActivityDescription { get; set; }
        public string ResponsibleId { get; set; }
        public string ResponsibleDescription { get; set; }
        public string PriorityId { get; set; }
        public string PriorityDescription { get; set; }
        public string ReferenceNo { get; set; }
        public string StateId { get; set; }
        public string StateDescription { get; set; }
        public string Confidential { get; set; }
        public string Code1 { get; set; }
        public string Code2 { get; set; }
        public string Note { get; set; }
    }

    public partial class AccountTransaction
    {
        public string Company { get; set; }
        public string FiscalYearId { get; set; }
        public string Period { get; set; }
        public string AccountNo { get; set; }
        public string VerificateNo { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountDeb { get; set; }
        public decimal AmountKred { get; set; }
        public string Text { get; set; }
        public bool Checked { get; set; }
        public string Date { get; set; }
    }

    //[MetadataType(typeof(Product.ProductDTO))]
    public partial class Product
    {
        [GarpShortFieldName("VKD", 1)]              //Variantkod
        public string VariantCode { get; set; }
        [GarpShortFieldName("HVL", 1)]              //Längd H-var
        public string HVL { get; set; }
        [GarpShortFieldName("MVL", 1)]              //Längd M-var
        public string MVL { get; set; }
        public int Id { get; set; }                 //Id är ett tillagt värde och finns inte i GARP
        [GarpShortFieldName("ANR", 13)]             //Aerikelnr
        public string ProductNo { get; set; }
        [GarpShortFieldName("BEN", 20)]             //Benämning
        public string Description { get; set; }
        [GarpShortFieldName("ENH", 3)]              //Enhet
        public string Unit { get; set; }
        [GarpShortFieldName("SUS", 1)]              //Lagerpåverkan
        public string StockUpdateType { get; set; }
        [GarpShortFieldName("TYP", 1)]              //Artikeltyp
        public string ProductType { get; set; }
        [GarpShortFieldName("APE", 7)]              //Antal per enhet (förp)
        public decimal? AmountPerUnit { get; set; }
        [GarpShortFieldName("SIP", 11)]             //Sen inpris
        public decimal? PurchasePrice { get; set; }
        [GarpShortFieldName("PRI", 11)]             //Försäljn pris
        public decimal? Price { get; set; }
        [GarpShortFieldName("XP1", 11)]             //Extra 1
        public decimal? ExtraPrice1 { get; set; }
        [GarpShortFieldName("XP2", 11)]             //Extra 2
        public decimal? ExtraPrice2 { get; set; }
        //        public decimal? PriceInStore { get; set; }
        [GarpShortFieldName("VIP", 11)]             //Vägt inpris
        public decimal? AverageCostPrice { get; set; }
        [GarpShortFieldName("SES", 2)]              //Säsong
        public string Season { get; set; }
        [GarpShortFieldName("LKD", 1)]              //Leveranskod
        public string DeliveryCode { get; set; }
        [GarpShortFieldName("TVK", 1)]              //Tillv/köp-kod
        public string OriginType { get; set; }
        [GarpShortFieldName("LGA", 11)]             //Lagersaldo
        public decimal? Stock { get; set; }
        [GarpShortFieldName("VPE", 9)]              //Vikt/enh
        public decimal? Weight { get; set; }
        [GarpShortFieldName("VOL", 9)]              //Volym/enh
        public decimal? Volyme { get; set; }
        [GarpShortFieldName("PLS", 6)]              //Lagerplacering
        public string WarehouseNo { get; set; }
        [GarpShortFieldName("MLG", 1)]              //Lagertyp
        public string WarehouseType { get; set; }
        [GarpShortFieldName("ADE", 3)]              //Antalsdecimaler
        public int? AmountDecimalCount { get; set; }
        [GarpShortFieldName("PDE", 3)]              //Prisdecimaler
        public int? PriceDecimalCount { get; set; }
        [GarpShortFieldName("ST1", 2)]              //Stat1
        public string ProductGroup1 { get; set; }
        [GarpShortFieldName("ST2", 2)]              //Stat2
        public string ProductGroup2 { get; set; }
        [GarpShortFieldName("ST3", 2)]              //Stat3
        public string ProductGroup3 { get; set; }
        [GarpShortFieldName("STN", 4)]              //Kortnr statnr
        public string ProductGroup4 { get; set; }
        [GarpShortFieldName("TLD", 6)]              //Extra_VG
        public string ProductGroup5 { get; set; }
        [GarpShortFieldName("KD1", 1)]              //Kod 1
        public string Code1 { get; set; }
        [GarpShortFieldName("KD2", 1)]              //Kod 2
        public string Code2 { get; set; }
        [GarpShortFieldName("KD3", 1)]              //Kod 3
        public string Code3 { get; set; }
        [GarpShortFieldName("KD4", 1)]              //Kod 4
        public string Code4 { get; set; }
        [GarpShortFieldName("KD5", 1)]              //Kod 5
        public string Code5 { get; set; }
        [GarpShortFieldName("KD6", 1)]              //Kod 6
        public string Code6 { get; set; }
        [GarpShortFieldName("KAT", 2)]              //Kod artikelkat
        public string Category { get; set; }
        [GarpShortFieldName("IKT", 2)]              //Kod ink-kto
        public string PurchaseAccount { get; set; }
        [GarpShortFieldName("KTO", 2)]              //Kod förs-kto
        public string SalesAccountId { get; set; }
        public string SalesAccount { get; set; }
        [GarpShortFieldName("WOB", 2)]              //Orderbehandlmod G3
        public string WOB { get; set; }
        public string Dimension { get; set; }
        public string Variant { get; set; }
        [GarpShortFieldName("URS", 2)]              //Ursprungsland
        public string CountryOfOrigin { get; set; }
        [GarpShortFieldName("MOM", 1)]              //Momskod
        public string VATCode { get; set; }
        [GarpShortFieldName("PRM", 2)]              //Prismodell
        public string PriceModelCode { get; set; }
        [GarpShortFieldName("RBK", 2)]              //Kod förs-rabatt
        public string DiscountSalesCode { get; set; }
        [GarpShortFieldName("IRK", 2)]              //Kod ink-rabatt
        public string DiscountPurchaseCode { get; set; }
        [GarpShortFieldName("VAL", 3)]              //Valuta f inpris
        public string PurchaseCurrencyCode { get; set; }
        [GarpShortFieldName("MTK", 2)]              //Materialkonto
        public string MaterialAccount { get; set; }
        [GarpShortFieldName("KUR", 1)]              //Kuranskod
        public string SaleableCode { get; set; }
        [GarpShortFieldName("OKV", 13)]             //Beställningskvantitet
        public string OrderQuantity { get; set; }
        [GarpShortFieldName("NLT", 5)]              //Ledtid
        public string LeadTime { get; set; }
        [GarpShortFieldName("BPT", 7)]              //Beställningspunkt
        public string OrderPoint { get; set; }
        [GarpShortFieldName("STP", 11)]             //Standardkalkyl
        public decimal? StandardPrice { get; set; }
        [GarpShortFieldName("ST1", 2)]              //Stat1 !!! Hämtas redan ovan som ProductGroup1
        public string ProductRange1 { get; set; }
        [GarpShortFieldName("ST2", 2)]              //Stat2 !!! Hämtas redan ovan som ProductGroup2
        public string ProductRange2 { get; set; }
        [GarpShortFieldName("BOL", 3)]              //Bolag - redovisn
        public string CompanyId { get; set; }
        [GarpShortFieldName("LNR", 10)]             //Leverantörsnr
        public string SupplierNo { get; set; }
        [GarpShortFieldName("BNR", 13)]             //Leverantörens best nr
        public string SuppliersProductNo { get; set; }
        [GarpShortFieldName("VMK", 1)]
        public string Trademark { get; set; }       //Nytt fält - Varumärke
        public List<Text> ProductText { get; set; }
        public List<WarehouseNumber> WarehouseList { get; set; }
        // BP_v2
        [GarpShortFieldName("ORA", 11)]             //Orderantal
        public decimal? QuantityInOrder { get; set; }
        [GarpShortFieldName("BEA", 11)]              //Beställt antal (exkl r
        public decimal? QuantityInPurchase { get; set; }
        [GarpShortFieldName("ROA", 11)]             //Orderantal (res)
        public decimal? QuantityInOrderReserved { get; set; }
        [GarpShortFieldName("RBA", 11)]             //Beställt antal (res)
        public decimal? QuantityInPurchaseReserved { get; set; }
        [GarpShortFieldName("INA", 11)]             //Inlev antal i år
        public decimal? DeliveredInThisYear { get; set; }
        [GarpShortFieldName("INF", 11)]             //Inlev antal fg år
        public decimal? DeliveredInLastYear { get; set; }
        [GarpShortFieldName("UTA", 11)]             //Utlev antal i år
        public decimal? DeliveredOutThisYear { get; set; }
        [GarpShortFieldName("UTF", 11)]             //Utlev antal fg år
        public decimal? DeliveredOutLastYear { get; set; }
        [GarpShortFieldName("SER", 6)]              //Senaste rörelsedatum
        public string LastMovementDate { get; set; }


    }

    public partial class ProductStock
    {
        public string ProductNo { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
        public decimal Stock { get; set; }
        public List<WarehouseNumber> WarehouseList { get; set; }
    }

    public partial class ProductMultiStock
    {
        public string ProductNo { get; set; }
        public string Description { get; set; }
        public decimal? Stock { get; set; }
        public string WarehouseNo { get; set; }
        public decimal? QuantityInOrder { get; set; }
        public decimal? QuantityInPurchase { get; set; }
    }

    public partial class Text
    {
        public string Field { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
    }

    public partial class WebText
    {
        public string Product { get; set; }
        public string Language { get; set; }
        public string ShortText { get; set; }
        public string LongText { get; set; }
        public List<string> ShortTextList { get; set; }
        public List<string> LongTextList { get; set; }
    }

    public partial class GetWebTextParam
    {
        public string ProductNo { get; set; }
        public string Language { get; set; }
    }

    public partial class OrderMatrix
    {
        public List<ModelVariant> ModelVariantList { get; set; }
    }

    public partial class ModelVariant
    {
        public ProductModel Model { get; set; }
        public ProductVariant Variant { get; set; }
        public List<Size> SizeList { get; set; }
    }

    public partial class Size
    {
        public ProductDimension Dimension { get; set; }
        public OrderRow OrderRow { get; set; }
    }

    public partial class ProductModel
    {
        public string ModelNo { get; set; }
        [Obsolete("User ProductObj instead")]
        public string Description { get; set; }
        public short ModelLenth { get; set; }
        public short VariantLength { get; set; }
        public short DimensionLength { get; set; }
        [Obsolete("User ProductObj instead")]
        public string Season { get; set; }
        [Obsolete("User ProductObj instead")]
        public string ProductGroup1 { get; set; }
        [Obsolete("User ProductObj instead")]
        public string ProductGroup2 { get; set; }
        [Obsolete("User ProductObj instead")]
        public string ProductGroup3 { get; set; }
        [Obsolete("User ProductObj instead")]
        public string ProductGroup4 { get; set; }
        [Obsolete("User ProductObj instead")]
        public string ProductGroup5 { get; set; }
        [Obsolete("User ProductObj instead")]
        public string Category { get; set; }
        [Obsolete("User ProductObj instead")]
        public string PurchaseAccount { get; set; }
        [Obsolete("User ProductObj instead")]
        public int PriceDecimalCount { get; set; }
        [Obsolete("User ProductObj instead")]
        public int AmountPerUnit { get; set; }
        [Obsolete("User ProductObj instead")]
        public List<Text> ProductText { get; set; }
        public List<ProductVariant> VariantList { get; set; }
        public Product ProductObj { get; set; }
    }

    // e.g colour
    public partial class ProductVariant
    {
        public string ModelNo { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public short ModelLenth { get; set; }
        public short VariantLength { get; set; }
        public short DimensionLength { get; set; }
        public List<Text> ProductText { get; set; }
        [Obsolete("User ProductObj instead")]
        public string Season { get; set; }
        public List<ProductDimension> DimensionList { get; set; }
        public Product ProductObj { get; set; }
    }

    // e.g size
    public partial class ProductDimension
    {
        public string ModelNo { get; set; }
        public string VariantCode { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public short ModelLenth { get; set; }
        public short VariantLength { get; set; }
        public short DimensionLength { get; set; }
        [Obsolete("User ProductObj instead")]
        public List<Text> ProductText { get; set; }
        public Product ProductObj { get; set; }
        public bool Active { get; set; }
        public string VariantDescription { get; set; }
    }

    public partial class ProductContainer
    {
        public Product Product { get; set; }
        public List<ProductDimension> Dimensions { get; set; }
        public List<ProductVariant> Variants { get; set; }
    }



    public partial class MaterialRow
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public int MaterialRowNo { get; set; }
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string ProductDescription { get; set; }
        public string ProductDescription2 { get; set; }
        public string Unit { get; set; }
        public decimal? Amount { get; set; }
        public decimal? DeliveredAmount { get; set; }
        public string DeliverState { get; set; }
        public string PreferedDeliverDate { get; set; }
        public bool FullyDeliverable { get; set; }
        public Product Product { get; set; }
        public string NoteStructure { get; set; }
        public string PositionNumber { get; set; }
        public int? NumberDecimal { get; set; }
    }

    public partial class MaterialRowMinimal
    {
        public int MaterialRowNo { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }
        public string WarehouseType { get; set; }
        public decimal Amount { get; set; }
        //        public List<WarehouseNumber> ProductWarehousNo { get; set; }
    }

    public partial class OperationRow
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public int OperationRowNo { get; set; }
        public string OperationNo { get; set; }
        public string OperationType { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string ProductionGroup { get; set; }
        public string Priority { get; set; }
        public string StartDate { get; set; }
        public string StopDate { get; set; }
        public decimal? PlannedAmount { get; set; }
        public decimal? ReportedTimeMan { get; set; }
        public decimal? ReportedTimeMachine { get; set; }
        public string Status { get; set; }
    }

    public partial class BaseTable
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string Type { get; set; }
        public string Code1 { get; set; }
        public string Code2 { get; set; }
        public string Code3 { get; set; }
        public string Code4 { get; set; }
        public string Code5 { get; set; }
        public string Code6 { get; set; }
        public string Code7 { get; set; }
        public string Code8 { get; set; }
        public string Code9 { get; set; }
        public string Code10 { get; set; }
        public string Code11 { get; set; }
        public string Code12 { get; set; }
        public int? Num1 { get; set; }
        public int? Num2 { get; set; }
        public int? Num3 { get; set; }
        public int? Num4 { get; set; }
        public int? Num5 { get; set; }
        public int? Num6 { get; set; }
        public int? Num7 { get; set; }
        public int? Num8 { get; set; }
        public int? Num9 { get; set; }
        public int? Num10 { get; set; }
        public int? Num11 { get; set; }
        public int? Num12 { get; set; }
    }

    public partial class Account
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Company { get; set; }
        //public string SRV { get; set; }
        public int AccountType { get; set; }
        public string Group1 { get; set; }
        public string Group2 { get; set; }
        public string Group3 { get; set; }
        public string Group4 { get; set; }
        public string Group5 { get; set; }
        public bool Active { get; set; }
    }

    public partial class PriceListDefinition
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Currency { get; set; }
    }

    public partial class WarehouseNumber
    {
        [GarpShortFieldName("LNR", 7)]                  			// Lagernr
        public string Id { get; set; }
        [GarpShortFieldName("ANR", 13)]                  			// Artikelnr
        public string ProductNo { get; set; }
        [GarpShortFieldName("ANM", 20)]                  			// Anmärkning
        public string Description { get; set; }
        [GarpShortFieldName("AM2", 6)]                  			// Anmärkning 2
        public string Description2 { get; set; }
        [GarpShortFieldName("BPT", 7)]                  			// Beställningspunkt
        public string OrderPoint { get; set; }
        [GarpShortFieldName("VIP", 11)]                  			// Vägt inpris
        public string VIP { get; set; }
        [GarpShortFieldName("OKV", 11)]                  			// Beställningskvantitet
        public decimal OrderQuantity { get; set; }
        [GarpShortFieldName("BEA", 11)]                  			// Beställt antal
        public decimal QuantityInPurchase { get; set; }
        [GarpShortFieldName("ORA", 11)]                  			// Orderantal
        public decimal QuantityInOrder { get; set; }
        [GarpShortFieldName("LGA", 11)]                  			// Lagersaldo
        public decimal Stock { get; set; }
    }

    public partial class ModeOfTransport
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }

    public partial class TransportTerm
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }

    public partial class OHText
    {
        public string DeliverNoteNo { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string OptionalInfo { get; set; }
        public string DeliverMark { get; set; }
    }

    public partial class Transport
    {
        public string DeliverNo { get; set; }
        public string OrderNo { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string ShipmentNo { get; set; }
        public string PackageId { get; set; }
        public string SenderReference { get; set; }
        public string OptionalInfo { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string DeliverMark { get; set; }
        public string DeliverMarkOH { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal TotalVolume { get; set; }
        public decimal TotalFlak { get; set; }
        public decimal PackageCount { get; set; }
        public string ModeOfTransportNo { get; set; }
        public string DeliverWayId { get; set; }
        public string DeliverWayDescription { get; set; }
        public string DeliverTermId { get; set; }
        public string DeliverTermDescription { get; set; }
        public string InternalDeliverMessage { get; set; }
        public string DeliverAddress1 { get; set; }
        public string DeliverAddress2 { get; set; }
        public string DeliverZipCity { get; set; }
        public string DeliverCountry { get; set; }
        public string PackageType1 { get; set; }
        public string PackageType2 { get; set; }
        public string PackageType3 { get; set; }
        public decimal? PackageAmount1 { get; set; }
        public decimal? PackageAmount2 { get; set; }
        public decimal? PackageAmount3 { get; set; }
        public decimal? Volume1 { get; set; }
        public decimal? Volume2 { get; set; }
        public decimal? Volume3 { get; set; }
        public decimal? Weight1 { get; set; }
        public decimal? Weight2 { get; set; }
        public decimal? Weight3 { get; set; }
        public string DeliverAnnounce { get; set; }
        public string DeliverAnnounceType { get; set; }
        public string DeliverAnnouncePhone { get; set; }
        public string OurReference { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string GoodsMeasurementType { get; set; }
        public string GoodsMark { get; set; }
        public string InvoiceState { get; set; }
        public string DeliveryNoteState { get; set; }
        public string DeliveryState { get; set; }
        public string X1F { get; set; }
        public string TransportTime { get; set; }
        public string TransportDocumentState { get; set; }
        public string PrinterUsername { get; set; }
        public string PrinterTerminal { get; set; }

        public List<ModeOfTransport> ModeOfTransportList { get; set; }
        public List<TransportTerm> TransportTermList { get; set; }
    }



    public partial class LogtradeForwarder
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public List<LogtradeProduct> Products { get; set; }
    }

    public partial class LogtradeProduct
    {
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string ForwarderCode { get; set; }
    }

    public partial class LogtradeTermOfDelivery
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public partial class LogtradeShipment
    {
        public string ProductCode { get; set; }
        public string SenderAddressId { get; set; }
        public string SenderReference { get; set; }
        public string ConsignmentId { get; set; }
        public string RecipientName { get; set; }
        public string RecipientAddress1 { get; set; }
        public string RecipientAddress2 { get; set; }
        public string RecipientAddress3 { get; set; }
        public string RecipientZip { get; set; }
        public string RecipientCity { get; set; }
        public string RecipientReference { get; set; }
        public string RecipientPhone { get; set; }
        public string RecipientFax { get; set; }
        public string RecipientContactPerson { get; set; }
        public string RecipientCountryCode { get; set; }
        public string DeliverNote { get; set; }
        public string TermsOfDeliveryCode { get; set; }
        public string OurReference { get; set; }
        public string GANNo { get; set; }
        public string DeliveryCustomerName { get; set; }
        public string DeliveryCustomerAddress1 { get; set; }
        public string DeliveryCustomerAddress2 { get; set; }
        public string DeliveryCustomerAddress3 { get; set; }
        public string DeliveryCustomerZip { get; set; }
        public string DeliveryCustomerCity { get; set; }
        public string DeliveryCustomerCountryCode { get; set; }
        public string DeliveryCustomerContact { get; set; }
        public string DeliveryCustomerContactPhone { get; set; }
        public decimal DeclaredValueField { get; set; }
        public string DeclaredCurrencyCodeField { get; set; }
        public PaymentType PaymentTypeField { get; set; }
        public LogtradeAuthorization Authorization { get; set; }
        public List<LogtradeGoodsItem> GoodsItemList { get; set; }
        public List<LogtradeShipmentService> ShipmentServiceList { get; set; }

    }

    public partial class LogtradeAuthorization
    {
        public string User { get; set; }
        public string Password { get; set; }
    }

    public partial class LogtradeSenderAddress
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public partial class LogtradeGoodsItem
    {
        public string PackageTypeCode { get; set; }
        public int Amount { get; set; }
        public decimal Weight { get; set; }
        public decimal Volyme { get; set; }
        public decimal LoadingMeters { get; set; }
    }

    public partial class LogtradePackageType
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public partial class LogtradeShipmentService
    {
        public string ServiceName { get; set; }
        public string Description { get; set; }
        public List<LogtradeProperty> PropertyDescriptions { get; set; }
    }

    public partial class LogtradeProperty
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Caption { get; set; }
    }



    public partial class ERPReport
    {
        public string DocGen { set; get; }
        public string Report { set; get; }
        public string Date { set; get; }
        public string IndexFrom { set; get; }
        public string IndexTo { set; get; }
        public string Medium { set; get; }
        public string Form { set; get; }
        public string FilePath { set; get; }
        public bool Wait { set; get; }
        public int WaitTimeoutSec { set; get; }
        public bool Collate { set; get; }
        public bool DirectPrint { set; get; }
        public int Copies { set; get; }

        public List<ERPReportDialog> Dialogs { get; set; }
    }

    public partial class UserConfiguration
    {
        public string Site { get; set; }
        public string ERP { get; set; }
        public string SocketServerAddress { get; set; }
        public string SocketServerPort { get; set; }
        public string SocketServerCompany { get; set; }
        public string MainDbDir { get; set; }
        public string ClientMainDbDir { get; set; }
        public string McPict { get; set; }
        public string LogPath { get; set; }
        public string InvoicePath { get; set; }
        public string PurchaseInvoicePath { get; set; }
        public int UserId { get; set; }
        public string GarpUser { get; set; }
        public string GarpPassword { get; set; }
        public string GarpRegKey { get; set; }
        public string GarpCompany { get; set; }
        public string CacheBasePath { get; set; }
    }



    public class ERPReportDialog
    {
        public string Id { get; set; }
        public string Value { get; set; }
    }

    public partial class CubDeliveryData
    {
        public string OrderNo { get; set; }
        public string SupplierNo { get; set; }
        public string SupplierName { get; set; }
        public List<string> OrderTextList { get; set; }
        public int RowNo { get; set; }
        public string ProductNo { get; set; }
        public string ProductDescription { get; set; }
        public string ProductDescription2 { get; set; }
        public decimal Amount { get; set; }
        public decimal DeliveredAmount { get; set; }
        public decimal DiscardAmount { get; set; }
        public string Quality { get; set; }
        public string Priority { get; set; }

        public List<OrderRowText> OrderRowTextList { get; set; }
    }

    public partial class PurchaseConnection
    {
        public string OrderNo { get; set; }
        public int RowNo { get; set; }
        public string ProductNo { get; set; }
        public string ConnectedOrderNo { get; set; }
        public int ConnectedRowNo { get; set; }
        public decimal Amount { get; set; }
        public decimal DeliveredAmount { get; set; }
        public string TypeOfConnection { get; set; }
    }

    public partial class FieldAnValues
    {
        public string Label { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
    }


    public partial class DeliverNoteOH
    {
        public string DeliverNoteNo { get; set; }
        public string CustomerNo { get; set; }
        public string InvoiceCustomerNo { get; set; }
        public string OrderType { get; set; }
        public string OrderConfirmationState { get; set; }
        public string PickListState { get; set; }
        public string DeliveryState { get; set; }
        public string X1F { get; set; }
        public string InvoiceState { get; set; }
        public string PaymentTerms { get; set; }
        public string DeliverWay { get; set; }
        public string TermsOfDeliveryCode { get; set; }
        public string OrderDate { get; set; }
        public string OurReference { get; set; }
        public string YourReference { get; set; }
        public string SellerId { get; set; }
        public string LastDeliverDate { get; set; }
        public string PreferedDeliverDate { get; set; }
        public string PriceListId { get; set; }
        public string CurrencyId { get; set; }
        public string CompanyId { get; set; }
        public string AccountNo { get; set; }
        public string CostPlaceId { get; set; }
        public string ProjectId { get; set; }
        public string ExtraId { get; set; }
        public string DiscountId { get; set; }
        public string VatId { get; set; }
        public string CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByTerminal { get; set; }
        public string OrderNo { get; set; }
        public string InvoiceNo { get; set; }
        public string DeliveryNoteState { get; set; }
        public string CurrencyValue { get; set; }
        public string TransportTime { get; set; }
        public string DiscountPercentage { get; set; }
        public SellerDTO Seller { get; set; }
        public string OurReferenceName { get; set; }
        public List<DeliverNoteRow> Rows { get; set; }
    }

    public partial class DeliverNoteRow
    {
        public string DeliverNoteNo { get; set; }
        public int RowNo { get; set; }
        public string OrderNo { get; set; }
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string ProductDescription { get; set; }
        public string ProductDescription2 { get; set; }
        public string Unit { get; set; }
        public string DeliverState { get; set; }
        public decimal DeliveredAmount { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public string DiscountPercentage { get; set; }
        public bool UseOHDiscount { get; set; }
        public decimal NetPrice { get; set; }
        public decimal RowValue { get; set; }
    }

    public class OrderStockObject
    {
        public decimal Saldo { get; set; }
        public decimal InStockToday { get; set; }
        public decimal InOrder { get; set; }
        public decimal InManufacturingOrder { get; set; }
        public decimal InMaterialOrder { get; set; }
        public decimal InPurchase { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
    }

    public class ProductStockResult
    {
        public decimal Balance { get; set; }
        public decimal InStockToday { get; set; }
        public decimal InOrder { get; set; }
        public decimal InManufacturingOrder { get; set; }
        public decimal InMaterialOrder { get; set; }
        public decimal InPurchase { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
    }

    //        OrderStockObject getProductAvailability(string uid, string productno, string warehouse, string fromDate, string calculate_method);

    public class ProductAvailabilityParam
    {
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string CalculateMethod { get; set; }
    }

    public class ProductOrderAmount
    {
        public decimal ManufacturingOrderAmount { get; set; }
        public decimal OrderAmount { get; set; }

    }
    public partial class FieldObject
    {
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
    }

    public partial class UpdateTableIndexObject
    {
        public string TableName { get; set; }
        public List<FieldObject> Fields { get; set; }
        public List<FieldObject> IndexFields { get; set; }
        public string Key { get; set; }
    }

    public partial class GetTableValueRequest
    {
        public string TableName { get; set; }
        public string Field { get; set; }
        public int Index { get; set; }
        public string Key { get; set; }
    }

    public partial class UpdateTableRangeObject
    {
        public string TableName { get; set; }
        public List<FieldObject> Fields { get; set; }
        public string RangeFrom { get; set; }
        public string RangeTo { get; set; }
    }


    public partial class AccountWithSum
    {
        public string AccountNo { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }
        public decimal Sum { get; set; }
    }

    public partial class AccountGroupWithSum
    {
        public string GroupNo { get; set; }
        public string Name { get; set; }
        public decimal Sum { get; set; }
        public List<AccountWithSum> AccountList { get; set; }
    }

    public partial class IncomeStatement
    {
        public string Company { get; set; }
        public string Year { get; set; }
        public string PeriodFrom { get; set; }
        public string PeriodTo { get; set; }
        public decimal Profit { get; set; }
        public decimal SumOfCost { get; set; }
        public decimal SumOfIncome { get; set; }
        public List<AccountGroupWithSum> IncomeGroupList { get; set; }
        public List<AccountGroupWithSum> CostGroupList { get; set; }
    }

    public partial class CustomerDiscountSalesCode
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string DescriptionExt { get; set; }
        public string Discount { get; set; }
    }
    public partial class PriceListPrice
    {
        public string ProductKey { get; set; }
        public string CustomerNo { get; set; }
        public string Id { get; set; }
        public string Fixed { get; set; }
        public string Alternative { get; set; }
        public decimal Price { get; set; }
        public decimal FixedPrice { get; set; }
        public string PriceMark { get; set; }
        public string MainDiscountCode { get; set; }
        public string DiscountCode { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }
        public string LastChanged { get; set; }
        public string ChangedByUser { get; set; }
        public string ChangedonTerminal { get; set; }
    }

    public partial class DiscountMatrixItem
    {
        public string Key { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public decimal DiscountPer1 { get; set; }
        public decimal DiscountPer2 { get; set; }
        public decimal DiscountPer3 { get; set; }
        public decimal DiscountPer4 { get; set; }
        public decimal DiscountPer5 { get; set; }
        public decimal DecimalsInRounding { get; set; }
        public decimal Discount1 { get; set; }
        public decimal Discount2 { get; set; }
        public decimal Discount3 { get; set; }
        public decimal Discount4 { get; set; }
        public decimal Discount5 { get; set; }
        public decimal Roudning { get; set; }
    }
    public partial class WarehouseBalanceResult
    {
        public int Qtyxml { get; set; }
        public int XmlSequence { get; set; }
        public List<WarehouseBalance> Balances { get; set; }
    }
    public partial class WarehouseBalance
    {
        public string Warehouse { get; set; }
        public string ProductNo { get; set; }
        public decimal Amount { get; set; }

    }

    public partial class Supplier
    {
        [GarpShortFieldName("LNR", 10)]                             // Levnr
        public string SupplierNo { get; set; }
        [GarpShortFieldName("NAM", 30)]                  	        // Namn
        public string Name { get; set; }
        [GarpShortFieldName("AD1", 30)]                  	        // Adress1
        public string Street { get; set; }
        [GarpShortFieldName("AD2", 30)]                  	        // Adress2
        public string Box { get; set; }
        public string Address3 { get; set; }                        // Hittar inte i SupplierSocket ... används kanske inte?
        [GarpShortFieldName("LND", 2)]                  	        // Landkod
        public string CountryId { get; set; }
        [GarpShortFieldName("FLN", 10)]                  	        // Fact/betmott
        public string InvoiceSupplierNo { get; set; }
        public string InvoiceSupplierName { get; set; }             // Utgår från att detta värde hämtas från en annan tabell mha föregående värde
        public string UnifiedInvoice { get; set; }                  // Hittar inte i SupplierSocket ... utgår från att den genereras eller hämtas från en annan tabell
        [GarpShortFieldName("ORT", 30)]                  	        // Postnr Ort
        public string ZipCity { get; set; }
        [GarpShortFieldName("BVK", 2)]                  	        // Bet Villkor
        public string PaymentTermsId { get; set; }
        [GarpShortFieldName("LVK", 2)]                  	        // Lev villkor
        public string DeliverTermsId { get; set; }
        [GarpShortFieldName("LSE", 2)]                  	        // Lev sätt
        public string DeliverWay { get; set; }
        [GarpShortFieldName("LSP", 1)]                  	        // Lev spärr
        public string DeliverLockId { get; set; }
        public string DeliverLockDescription { get; set; }          // Utgår från att den fylls i från annan tabell utifrån värdet av ovan sträng
        [GarpShortFieldName("SPR", 1)]                  	        // Språkkod
        public string LanguageCode1 { get; set; }
        [GarpShortFieldName("SP1", 1)]                  	        // Språkkod extra
        public string LanguageCode2 { get; set; }
        [GarpShortFieldName("SLJ", 2)]                  	        // Inköpare
        public string SellerId { get; set; }
        [GarpShortFieldName("VAL", 3)]                  	        // Valutakod
        public string CurrencyId { get; set; }
        [GarpShortFieldName("KAT", 2)]                  	        // Lev kategori
        public string CategoryId { get; set; }
        public string CategoryDescription { get; set; }             // Utgår från att den fylls i från annan tabell utifrån värdet av ovan sträng
        [GarpShortFieldName("IKT", 2)]                  	        // Kontokod inköporder
        public string AccountId { get; set; }
        [GarpShortFieldName("LTY", 1)]                              // Leverantörstyp
        [Obsolete]
        public string CustomerType { get; set; }
        [GarpShortFieldName("LTY", 1)]                              // Leverantörstyp
        public string SupplierType { get; set; }
        [GarpShortFieldName("TEL", 18)]                  	        // Telefonnr
        public string Phone { get; set; }
        [GarpShortFieldName("FAX", 18)]                  	        // Fax-nr
        public string Fax { get; set; }
        [GarpShortFieldName("REF", 20)]                  	        // Referens
        public string Reference { get; set; }
        [GarpShortFieldName("ORG", 10)]                  	        // Organisationnr
        public string CorporateIdentityNo { get; set; }
        public string Text { get; set; }                            // Fanns inte med i SupplierSocket ... Kanske den beräknas med utgångspunkt av nedan 7 textrader?
        [GarpShortFieldName("TX1", 20)]                  	        // Text 1
        public string Text_1 { get; set; }
        [GarpShortFieldName("TX2", 20)]                  	        // Text 2
        public string Text_2 { get; set; }
        [GarpShortFieldName("TX3", 20)]                  	        // Text 3
        public string Text_3 { get; set; }
        [GarpShortFieldName("TX4", 20)]                  	        // Text 4
        public string Text_4 { get; set; }
        [GarpShortFieldName("TX5", 20)]                  	        // Text 5
        public string Text_5 { get; set; }
        [GarpShortFieldName("TX6", 20)]                  	        // Text 6
        public string Text_6 { get; set; }
        [GarpShortFieldName("TX7", 20)]                  	        // Text7
        public string Text_7 { get; set; }
        public string DeliverZip { get; set; }                      // Hittar inte i SupplierSocket ... kanske inte används eller fylls i med ovan värde från ZipCity som hämtar värde från ORT
        [GarpShortFieldName("RBK", 1)]                  	        // Rabattkod
        public string DiscountId { get; set; }
        [GarpShortFieldName("RDB", 1)]                  	        // Ränta deb
        public string InterestInvoice { get; set; }
        public string ClaimCode { get; set; }                       // Hittar inte i SupplierSocket ... Beräknas? Används inte?
        public string DutyFree { get; set; }                        // Hittar inte i SupplierSocket ... Beräknas? Används inte?
        [GarpShortFieldName("SER", 6)]                  	        // Sen rörelse
        public string LastInvoiceDate { get; set; }
        [GarpShortFieldName("MOM", 1)]                  	        // Momskod
        public string VATId { get; set; }
        public Nullable<decimal> CreditLimit { get; set; }          // Hittar inte i SupplierSocket ... beräknas?
        public Nullable<decimal> CalculatedCreditTime { get; set; } // Hittar i SupplierSocket som KVD men KVD finns inte med på min lista över poster i tabellen Leverantörer
        [GarpShortFieldName("SLD", 15)]                  			// Öppet saldo
        public Nullable<decimal> OpenAmount { get; set; }
        [GarpShortFieldName("ODA", 15)]                  			// Omsätt detta år
        public Nullable<decimal> TurnoverThisYear { get; set; }
        [GarpShortFieldName("OFA", 15)]                  			// Omsätt fg år
        public Nullable<decimal> TurnoverLastYear { get; set; }
        public Nullable<decimal> ContributionMarginThisYear { get; set; }   // Hittar inte i SupplierSocket
        public Nullable<decimal> ContributionMarginLastYear { get; set; }   // Hittar inte i SupplierSocket
        [GarpShortFieldName("TPT", 9)]                  	        // Transporttid
        public string TransportTime { get; set; }
        [GarpShortFieldName("NM1", 11)]                  			// Extra num fält 1
        public Nullable<decimal> Numeric_1 { get; set; }
        [GarpShortFieldName("NM2", 11)]                  			// Extra num fält 2
        public Nullable<decimal> Numeric_2 { get; set; }
        [GarpShortFieldName("NM3", 11)]                  			// Extra num fält 3
        public Nullable<decimal> Numeric_3 { get; set; }
        [GarpShortFieldName("NM4", 11)]                  			// Extra num fält 4
        public Nullable<decimal> Numeric_4 { get; set; }
        [GarpShortFieldName("KD1", 1)]                  			// Kod 1
        public string Code_1 { get; set; }
        [GarpShortFieldName("KD2", 1)]                  			// Kod 2
        public string Code_2 { get; set; }
        [GarpShortFieldName("KD3", 1)]                  			// Kod 3
        public string Code_3 { get; set; }
        [GarpShortFieldName("KD4", 1)]                  			// Kod 4
        public string Code_4 { get; set; }
        [GarpShortFieldName("KD5", 1)]                  			// Kod 5
        public string Code_5 { get; set; }
        [GarpShortFieldName("KD6", 1)]                  			// Kod 6
        public string Code_6 { get; set; }
        [GarpShortFieldName("GPN", 6)]                  			// Godspostnr
        public string GoodsZip { get; set; }
        [GarpShortFieldName("BGK", 15)]                  			// Bankgironr
        public string BankGiro { get; set; }
        [GarpShortFieldName("PGK", 15)]                  			// Postgironr
        public string PostGiro { get; set; }

    }

    public partial class SupplierMinimal
    {
        public string SupplierNo { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string Box { get; set; }
        public string ZipCity { get; set; }
        public string CategoryId { get; set; }
        public string CategoryDescription { get; set; }
        public string CustomerType { get; set; }
        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public string DeliverLockId { get; set; }
        public string SupplierId { get; set; }
        public string SupplierName { get; set; }
        public Nullable<decimal> OpenAmount { get; set; }

        public List<FieldAnValues> CustomFieldList { get; set; }
    }

    public partial class OrderHeadFreight
    {
        public string OrderNo { get; set; }
        //public int Orderserie { get; set; }
        //public string RecordType { get; set; }
        public string Freight20_1 { get; set; }
        public string Freight20_2 { get; set; }
        public string Freight20_3 { get; set; }
        public string Freight15 { get; set; }
        public string Freight25 { get; set; }
        public string Flag1 { get; set; }
        public string Flag2 { get; set; }
        public string Flag3 { get; set; }
        public string Flag4 { get; set; }
        public string Flag5 { get; set; }
        public int? Num1 { get; set; }
        public int? Num2 { get; set; }
        public int? Num3 { get; set; }
        public int? Num4 { get; set; }
        public int? Num5 { get; set; }
    }

    public class OrderStatistic
    {
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public decimal OrderValue { get; set; }
        public int NoOfOrders { get; set; }
        public int NoOfOrderRows { get; set; }
        public int NoOfUniqeProducts { get; set; }
    }

    public class SellerInvoiceStatistic
    {
        public string SellerId { get; set; }
        public string SellerName { get; set; }
        public List<CustomerInvoiceStatistic> CustomerList { get; set; }
    }

    public class CustomerInvoiceStatistic
    {
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public decimal InvoicedAmount { get; set; }
        public decimal InvoicedAmountLastYear { get; set; }
    }

    public class CustomerHistory
    {
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string SellerId { get; set; }
        public string Date { get; set; }
        public string WarehouseNo { get; set; }
        public string OrderNo { get; set; }
        public string DeliverNoteNo { get; set; }
        public string TransactionType { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceNo { get; set; }
        public string Price { get; set; }
        public decimal GrossPrice { get; set; }
        public decimal GrossAmount { get; set; }
        public decimal NetAmount { get; set; }
        public decimal Quantity1 { get; set; }
        public decimal Quantity2 { get; set; }
        public string Company { get; set; }
        public string InvoiceCustomerNo { get; set; }
        public string OrderType { get; set; }
        public string RowType { get; set; }

    }

    public class ProductStatistic
    {
        //Kundnr / Namn / Antal artikelnummer / Antal orderrader
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public int UniqueProducts { get; set; }
        public int NoOfOrderRows { get; set; }
    }

    public class LogModel
    {
        public string LogType { get; set; }
        public string User { get; set; }
        public string Terminal { get; set; }
        public string LogDate { get; set; }
        public string LogTime { get; set; }
        public string Table { get; set; }
        public int KeyLength { get; set; }
        public string KeyValue { get; set; }
        public List<DbLogField> FieldList { get; set; }
    }

    public class LogModelFlattened
    {
        public string LogType { get; set; }
        public string User { get; set; }
        public string Terminal { get; set; }
        public string LogDate { get; set; }
        public string LogTime { get; set; }
        public string Table { get; set; }
        public int KeyLength { get; set; }
        public string KeyValue { get; set; }
        public string Field { get; set; }
        public int ValueBeforeLength { get; set; }
        public int ValueAfterLength { get; set; }
        public string ValueBefore { get; set; }
        public string ValueAfter { get; set; }
    }

    public class DbLogField
    {
        public string Field { get; set; }
        public int ValueBeforeLength { get; set; }
        public int ValueAfterLength { get; set; }
        public string ValueBefore { get; set; }
        public string ValueAfter { get; set; }
    }

    public class MaterialOrder
    {
    }

    public class LogTable
    {
        public LogTable()
        {
            this.Fields = new List<LogField>();
        }

        public string Name { get; set; }
        public List<LogField> Fields { get; set; }
    }

    public class LogField
    {
        public string Name { get; set; }
    }

    public class GISPaymentType
    {

        public int Id { get; set; }
        public string Description { get; set; }
    }

    public class ErrandFilter
    {

        public string CustomerFrom { get; set; }
        public string CustomerTo { get; set; }
        public string Activity { get; set; }
        public string Responsible { get; set; }
        public string Priority { get; set; }
        public string State { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }

    }

 
    public partial class TimeReportParam
    {
        public short Index { get; set; }
        public string IndexFilter { get; set; }
        public string GeneralFilter { get; set; }
    }

    public partial class TimeReportStartOrderParam
    {
        public string EmployeeId { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public string OrderNo { get; set; }
        public string RowNo { get; set; }
        public string OperationNo { get; set; }
        public string Text { get; set; }
    }

    public partial class TimeReportStartOrderResult
    {
        public string EmployeeId { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public string OrderNo { get; set; }
        public string RowNo { get; set; }
        public string OperationNo { get; set; }
        public string Text { get; set; }
        public bool Succeeded { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    public partial class TimeReport
    {
        public string TransactionType { get; set; }
        public string HistoryNo { get; set; }
        public int RowNo { get; set; }
        public string EmployeeNo { get; set; }
        public string ProductionGroup { get; set; }
        public string OperationNo { get; set; }
        public string CheckinTime { get; set; }
        public string CheckoutTime { get; set; }
        public string ProductNo { get; set; }
        public string WarehouseNo { get; set; }
        public string CustomerNo { get; set; }
        public string Company { get; set; }
        public string Date { get; set; }
        public string TimeCode { get; set; }
        public string OrderNo { get; set; }
        public string ScrapCause { get; set; }
        public string Origin { get; set; }
        public decimal ReportedAmount { get; set; }
        public decimal ScrapAmount { get; set; }
        public decimal ReportedManTime { get; set; }
        public decimal ReportedMachineTime { get; set; }
        public decimal PlanedManTime { get; set; }
        public decimal PlanedMachineTime { get; set; }
        public decimal ReportedManCost { get; set; }
        public decimal ReportedMachineCost { get; set; }
        public decimal PlanedCost { get; set; }
        public string StatisticFlagg { get; set; }
        public string InfoFlagg { get; set; }
        public string ShortCode { get; set; }
    }

    public class ConceptSearchParam
    {
        public string Concept { get; set; }
        public string ConceptExtra1 { get; set; }
        public string ConceptExtra2 { get; set; }
        public string ConceptExtra3 { get; set; }
        public bool IncludeFrozenArticles { get; set; }
    }

    public class ProductConcept
    {
        public string Concept { get; set; }
        public string ProductNo { get; set; }
    }

    public class ProductConceptListParam
    {
        public string ProductNo { get; set; }
    }


    public partial class CustomerParam
    {
        public string Prefix { get; set; }
        public bool Autogenerate { get; set; }
        public Customer Customer { get; set; }
        public string Email { get; set; }
        public bool CheckEmail { get; set; }
        public bool CheckOnlyGeneralMail { get; set; }

    }

    public class WarehouseBalancingParam
    {
        public string ProductNoFrom { get; set; }
        public string ProductNoTo { get; set; }
    }
    public partial class AddCustomerResult
    {
        public string NewCustomerNo { get; set; }
        public string FoundCustomerNo { get; set; }
    }

    public partial class AddSupplierResult
    {
        public string NewSupplierNo { get; set; }
        public string FoundSupplierNo { get; set; }
    }

    public class WarehouseBalancingResult
    {
        public string ProductsChanged { get; set; }
        public string ProductsNotChanged { get; set; }
    }

    public class ProductBalancingParam
    {
        public string ProductNoFrom { get; set; }
        public string ProductNoTo { get; set; }

    }


    public class ProductBalancingResult
    {
        public string ProductsChanged { get; set; }
        public string ProductsNotChanged { get; set; }

    }

    public class ProductMinAvailabilityParam
    {
        public string ProductNo { get; set; }
        public string DateTo { get; set; }
        public string WarehouseNo { get; set; }
    }

    public class ProductMinAvailabilityListParam
    {
        public string ProductFrom { get; set; }
        public string ProductTo { get; set; }
        public string DateTo { get; set; }
        public string WarehouseNo { get; set; }
    }

    public class ProductMinAvailableResult
    {
        public string ProductNo { get; set; }
        public string DateWithMinAvailability { get; set; }
        public decimal AvailableAmountOnDate { get; set; }
    }

    public class ProductStructureParam
    {
        public string ParentProductNo { get; set; }
        public string PositionNo { get; set; }
        public string ChildProductNo { get; set; }
        public decimal? QuantityPerUnit { get; set; }
        public string StructureNote { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string OperationNo { get; set; }
        public string CalculateType { get; set; }
        public string Brytner { get; set; }
        public string MaterialFlag { get; set; }
        public string CalculateFlag { get; set; }
        public decimal? OverflowPercent { get; set; }
    }


    public class ProductStructure
    {
        public string ParentProductNo { get; set; }
        public string PositionNo { get; set; }
        public string ChildProductNo { get; set; }
        public decimal? QuantityPerUnit { get; set; }
        public string StructureNote { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string OperationNo { get; set; }
        public string CalculateType { get; set; }
        public string Brytner { get; set; }
        public string MaterialFlag { get; set; }
        public string CalculateFlag { get; set; }
        public decimal? OverflowPercent { get; set; }
    }

    public class OrderResult<T>
    {
        public string Message { get; set; }
        public string ErrorMessage { get; set; }
        public bool Succeeded { get; set; }
        public T Result { get; set; }
        public List<OrderRowResult> RowResult { get; set; }
    }

    public class OrderRowResult
    {
        public string Message { get; set; }
        public OrderRow Row { get; set; }
        public bool Succeeded { get; set; }
    }

    public partial class SendMailParam
    {
        public string FromEmail { get; set; }
        public string FromName { get; set; }
        public List<SendMailReceipent> To { get; set; }
        public string Subject { get; set; }
        public string MessageText { get; set; }
        public string MessageHtml { get; set; }
        public List<SendMailReceipent> Cc { get; set; }
        public List<SendMailReceipent> Bcc { get; set; }
        public List<SendMailAttachment> Attachments { get; set; }
    }

    public partial class SendMailMultipleParam
    {
        public string FromEmail { get; set; }
        public string FromName { get; set; }
        public List<SendMailReceipent> To { get; set; }
        public string Subject { get; set; }
        public string MessageText { get; set; }
        public string MessageHtml { get; set; }
        public List<SendMailReceipent> Cc { get; set; }
        public List<SendMailReceipent> Bcc { get; set; }
        public List<SendMailAttachment> Attachments { get; set; }
    }

    public partial class OrderListParam
    {
        public short Index { get; set; }
        public string Range { get; set; }
        public string Filter { get; set; }
        public string OGRFilter { get; set; }
        public bool ReverseReading { get; set; }
        public bool WithRows { get; set; }
        public bool WithProductObject { get; set; }
        public short MaxCount { get; set; }
        //public CacheParam Cache { get; set; }
    }

    /// <summary>
    /// Max filesize is 15mb and needs to be base64 ecnoded
    /// </summary>
    public partial class SendMailAttachment
    {
        public string ContentType { get; set; }
        public string Filename { get; set; }
        public string Base64 { get; set; }
    }
    public partial class SendMailReceipent
    {
        public string Email { get; set; }
        public string Name { get; set; }
    }

    [System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple = true)]
    public class GarpShortTableName : System.Attribute
    {
        string name;

        public GarpShortTableName(string name)
        {
            this.name = name;
        }

        public string GetName()
        {
            return name;
        }
    }

    [System.AttributeUsage(System.AttributeTargets.Property, AllowMultiple = true)]
    public class GarpShortFieldName : System.Attribute
    {
        string mName;
        short mLength;

        public GarpShortFieldName(string name, short length)
        {
            this.mName = name;
            this.mLength = length;
        }

        public string GetName()
        {
            return mName;
        }
    }

    public enum PaymentType
    {

        AnyPayment,
        Sender,
        Recipient,
        ThirdParty,
        UnknownParty,
    }
}
