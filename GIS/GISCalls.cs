﻿using GIS_Dto;
using log4net;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace TexlaEDIParser.GIS
{
    public static class GISCalls
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(Program));

        public static GIS_Dto.OrderHead GetOrderById(string id, bool with_rows)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";
            OrderHead oh = null;

            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    rest = getAddress("OrderSvc", "/REST/GetOrderById") + @"/" + token + @"/" + id + @"/" + with_rows.ToString();

                    logger.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    oh = JsonConvert.DeserializeObject<OrderHead>(response);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }

            return oh;
        }

        public static GIS_Dto.OrderRow GetOrderRow(string onr, string row)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";
            OrderRow oh = null;

            try
            {
                rest = getAddress("OrderSvc", "/REST/GetOrderRow") + @"/" + token + @"/" + onr + @"/" + row;

                logger.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                oh = JsonConvert.DeserializeObject<OrderRow>(response);
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }

            return oh;
        }

        public static List<OrderHead> GetOrderByIdxList(OrderListParam param)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            List<OrderHead> result = new List<OrderHead>();

            string rest = "";
            try
            {
                rest = getAddress("v2/OrderSvc", "/REST/GetOrderList") + @"/" + token;

                logger.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string json = reader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<List<OrderHead>>(json);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }

            return result;
        }

        //public static List<OrderHead> V2_GetOrderByIdxList(OrderListParam param)
        //{
        //    string token = GenericFunctions.GetSettingsValue("GISToken");
        //    string rest = "";

        //    List<OrderHead> lst = new List<OrderHead>();

        //    try
        //    {
        //        if (param.Index != null)
        //        {
        //            string escIdxFilter = System.Net.WebUtility.UrlEncode(idxfilter);

        //            rest = getAddress("OrderSvc", "/REST/GetOrderByIdxList") + @"/" + token + @"/" + idx + @"/" + escIdxFilter + @"/" + filter + @"/" + with_rows.ToString() + @"/" + max_count + @"/" + reverse_reading.ToString();

        //            logger.Debug("CALL TO: " + rest);

        //            WebRequest request = WebRequest.Create(rest);
        //            request.Timeout = 3600000;

        //            WebResponse ws = request.GetResponse();
        //            Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //            StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //            string response = responseStream.ReadToEnd();
        //            responseStream.Close();

        //            lst = JsonConvert.DeserializeObject<List<OrderHead>>(response);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("Error in call: " + rest, e);
        //    }

        //    return lst;
        //}

        public static OrderResult<OrderHead> AddOrderHead(OrderHead oh)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            OrderResult<OrderHead> result = new OrderResult<OrderHead>();

            string rest = "";
            try
            {
                rest = getAddress("v2/OrderSvc", "/REST/AddOrder") + @"/" + token;

                logger.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(oh));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string json = reader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<OrderResult<OrderHead>>(json);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public static void UpdateOrderHead(OrderHead oh)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";
            try
            {
                rest = getAddress("OrderSvc", "/REST/UpdateOrderHead") + @"/" + token;
                logger.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(oh));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }
        }

        public static OrderResult<string> UpdateOrderRow(OrderRow or)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";
            OrderResult<string> orderresult = new OrderResult<string>();

            try
            {
                rest = getAddress("OrderSvc", "/REST/UpdateOrderRow") + @"/" + token;
                logger.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(or));
                }

                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string result = reader.ReadToEnd();
                        orderresult = JsonConvert.DeserializeObject<OrderResult<string>>(result);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }

            return orderresult;
        }

        public static void AddOrderRow(List<OrderRow> orList)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";

            try
            {
                rest = getAddress("OrderSvc", "/REST/UpdateOrderRowList") + @"/" + token;
                logger.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(orList));
                }

                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string result = reader.ReadToEnd();
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }

        }

        public static OrderResult<string> DeleteOrderRow(OrderRow or)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";
            OrderResult<string> orderresult = new OrderResult<string>();

            try
            {
                rest = getAddress("OrderSvc", "/REST/DeleteOrderRow") + @"/" + token;

                logger.Debug("CALL TO: " + rest);
                logger.Debug("With payload:" + JsonConvert.SerializeObject(or));

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(or));
                }

                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string result = reader.ReadToEnd();
                        orderresult = JsonConvert.DeserializeObject<OrderResult<string>>(result);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }

            return orderresult;
        }

        public static void DeleteOrderHead(OrderHead oh)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";

            try
            {
                rest = getAddress("OrderSvc", "/REST/DeleteOrderHead") + @"/" + token;
                logger.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(oh));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }
        }

        //public static Product GetProductById(string id)
        //{
        //    string token = GenericFunctions.GetSettingsValue("GISToken");
        //    string rest = "";
        //    Product product = new Product();

        //    rest = getAddress("ProductSvc", "/REST/GetProduct") + "/" + token + @"/" + id;

        //    logger.Debug("CALL TO: " + rest);

        //    try
        //    {
        //        WebRequest request = WebRequest.Create(rest);

        //        WebResponse ws = request.GetResponse();
        //        //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
        //        StreamReader responseStream = new StreamReader(ws.GetResponseStream());
        //        string response = responseStream.ReadToEnd();
        //        responseStream.Close();

        //        product = JsonConvert.DeserializeObject<Product>(response);
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("Error in call: " + rest, e);
        //    }

        //    return product;
        //}

        public static List<Product> GetProductList(string index, string indexfilter, string general_filter_string)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";
            List<Product> lst = new List<Product>();

            try
            {
                rest = getAddress("ProductSvc", "/REST/GetProductList") + "/" + token + "/" + index + "/" + indexfilter + "/" + general_filter_string;

                logger.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<Product>>(response);
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public static Customer GetCustomerById(string id)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";
            Customer customer = new Customer();

            rest = getAddress("CustomerSvc", "/REST/GetCustomer") + "/" + token + @"/" + id;
            logger.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                //Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                customer = JsonConvert.DeserializeObject<Customer>(response);
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }

            return customer;
        }

        public static List<Customer> GetCustomerList(string index, string indexfilter, string general_filter_string)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";
            List<Customer> lst = new List<Customer>();

            try
            {
                rest = getAddress("CustomerSvc", "/REST/GetCustomerList") + "/" + token + "/" + index + "/" + indexfilter + "/" + general_filter_string;

                logger.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                //Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<Customer>>(response);
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public static List<CustomerHistory> GetHistoryOnCustomerList(string customer_from, string customer_to, string date_from, string date_to)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";

            List<CustomerHistory> lst = new List<CustomerHistory>();

            try
            {
                rest = getAddress("HistorySvc", "/REST/GetHistoryOnCustomerList") + "/" + token + @"/" + customer_from + @"/" + customer_to + @"/" + date_from + @"/" + date_to;

                logger.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Timeout = 3600000;

                WebResponse ws = request.GetResponse();
                //Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<CustomerHistory>>(response);
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public static UserErpInfo GetERPInfo(string user)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";
            UserErpInfo result = new UserErpInfo();

            rest = getAddress("RebornSystem", "/REST/GetUserErpInfo") + "/" + token + @"/" + user;
            logger.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                //Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<UserErpInfo>(response);
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public static void SendMail(SendMailParam mailparam)
        {
            string token = GenericFunctions.GetSettingsValue("GISToken");
            string rest = "";
            
            try
            {
                rest = getAddress("MailSvc", "/REST/SendMail");
                logger.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(mailparam));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in call: " + rest, e);
            }
        }

        private static string getAddress(string service, string rest_function)
        {
            return getGISBaseAddress() + getGISEndpointAddress(service) + rest_function;
        }

        private static string getGISBaseAddress()
        {
            string result = "";

            try
            {
                var rootDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);//System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);

                var builder = new ConfigurationBuilder().SetBasePath(rootDir).AddJsonFile("appsettings.json");
                var configuration = builder.Build();
                
                result = configuration["GISBaseAddress"];

            }
            catch (Exception e)
            {
                logger.Error("Error in getGISBaseAddress", e);
            }
            return result;
        }

        public static string getGISEndpointAddress(string endpoint)
        {
            string result = endpoint;

            try
            {
                string hostingType  = GenericFunctions.GetSettingsValue("ServiceHostingType");

                if (hostingType.Equals(""))
                    hostingType = "Standalone";

                else if (hostingType.Equals("IIS"))
                {
                    result = endpoint += ".svc";
                }
            }
            catch
            {
                
            }

            return result;
        }

    }
}
