﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GIS_Dto;
using log4net;
using Microsoft.Extensions.Logging;
using TexlaEDIParser.Models;
using Microsoft.EntityFrameworkCore;

namespace TexlaEDIParser
{
    public static class SqlDb
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(Program));

        public static void AddMatch(Match match)
        {
            logger.Info("Running AddMatch");

            try
            {
                using (var context = new Models.TexlaEDIContext())
                {
                    context.Match.Add(match);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.AddMatch()", e);
            }
        }

        public static void AddGarpOrderList(List<GarpOrderBatch> rows)
        {
            logger.Info("Running AddMatch");

            try
            {
                using (var context = new Models.TexlaEDIContext())
                {
                    context.GarpOrderBatch.AddRange(rows.ToArray());
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.AddGarpOrderList()", e);
            }
        }

        public static void AddGarpHistoryList(List<GarpHistoryBatch> rows)
        {
            logger.Info("Running AddMatch");

            try
            {
                using (var context = new Models.TexlaEDIContext())
                {
                    context.GarpHistoryBatch.AddRange(rows.ToArray());
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.AddGarpHistoryList()", e);
            }
        }

        public static PlanFile AddPlanFile(PlanFile plan)
        {
            logger.Info("Running AddPlanFile");

            try
            {
                using (var context = new Models.TexlaEDIContext())
                {
                    context.PlanFile.Add(plan);
                    context.SaveChanges();
                    context.Entry(plan).Reload();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.AddPlan()", e);
            }

            return plan;
        }

        public static PlanFile UpdatePlanFile(PlanFile plan)
        {
            logger.Info("Running UpdatelanFile");

            try
            {
                using (var context = new Models.TexlaEDIContext())
                {
                    context.PlanFile.Update(plan);
                    context.SaveChanges();
                    context.Entry(plan).Reload();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.UpdatePlan()", e);
            }

            return plan;
        }

        public static void DeleteMatch(Match match)
        {
            logger.Info("Running AddMatch");

            try
            {
                using (var context = new Models.TexlaEDIContext())
                {
                    context.Match.Add(match);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.DeleteMatch()", e);
            }
        }

        public static PlanFile GetPlanFile(int id)
        {
            PlanFile result = null;

            Console.WriteLine("Running PlanFile");
            logger.Info("Running PlanFile");

            try
            {
                using (var db = new Models.TexlaEDIContext())
                {
                    result = db.PlanFile.Include("PlanProduct").Where(p => p.Id == id)?.First();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in SqlDb.PlanFile() : " + e.Message);
                logger.Error("Error in SqlDb.PlanFile()", e);
            }

            return result;
        }

        public static List<PlanFile> GetPlanFileList(DateTime from)
        {
            List<PlanFile> result = null;

            logger.Info("Running PlanFile");

            try
            {
                using (var db = new Models.TexlaEDIContext())
                {
                    result = db.PlanFile.Include("PlanProduct").Where(p => p.FileDate.Value.Date >= from.Date)?.ToList();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.PlanFile()", e);
            }

            return result;
        }

        public static List<PlanFile> GetAllPlanFilesNotUpdatedToGarp()
        {
            List<PlanFile> result = null;

            logger.Info("Running PlanFile");

            try
            {
                using (var db = new Models.TexlaEDIContext())
                {
                    result = db.PlanFile.Include("PlanProduct").Where(p => p.UpdatedToGarp != true)?.ToList();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.PlanFile()", e);
            }

            return result;
        }

        public static PlanProduct GetPlanProduct(int id)
        {
            PlanProduct result = null;

            logger.Info("Running GetPlanProduct");

            try
            {
                using (var db = new Models.TexlaEDIContext())
                {
                    result = db.PlanProduct.Include(p => p.PlanDetail).Where(p => p.Id == id).First();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.GetMatchByPlanBatch()", e);
            }

            return result;
        }

        public static List<Match> GetMatchByPlanProduct(int planproductid)
        {
            List<Match> result = null;

            logger.Info("Running GetMatchByPlanBatch");

            try
            {
                using (var db = new Models.TexlaEDIContext())
                {
                    result = db.Match.Where(p => p.PlanProductId == planproductid)?.ToList();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.GetMatchByPlanBatch()", e);
            }

            return result;
        }

        public static List<GarpOrderBatch> GetGarpOrderBatchList(int planproductid)
        {
            List<GarpOrderBatch> result = null;

            logger.Info("Running GetMatchByGarpOrderBatch");

            try
            {
                using (var db = new Models.TexlaEDIContext())
                {
                    result = db.GarpOrderBatch.Where(g => g.PlanProductId == planproductid).OrderBy(g=>g.DeliverDate)?.ToList();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.GetGarpOrderBatchList()", e);
            }

            return result;
        }

        public static GarpOrderBatch GetGarpOrderBatch(int batchid)
        {
            GarpOrderBatch result = null;

            logger.Info("Running GetGarpOrderBatch");

            try
            {
                using (var db = new Models.TexlaEDIContext())
                {
                    result = db.GarpOrderBatch.Find(batchid);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.GetGarpOrderBatch()", e);
            }

            return result;
        }

        public static List<GarpHistoryBatch> GetGarpHistoryBatchList(int planproductid)
        {
            List<GarpHistoryBatch> result = null;

            logger.Info("Running GetMatchByGarpOrderBatch");

            try
            {
                using (var db = new Models.TexlaEDIContext())
                {
                    result = db.GarpHistoryBatch.Where(g => g.PlanProductId == planproductid)?.ToList();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.GetGarpHistoryBatchList()", e);
            }

            return result;
        }

        public static List<Match> GetMatchByGarpOrderBatch(int batchid)
        {
            List<Match> result = null;

            logger.Info("Running GetMatchByGarpOrderBatch");

            try
            {
                using (var db = new Models.TexlaEDIContext())
                {
                    result = db.Match.Where(p => p.GarpOrderBatchId == batchid)?.ToList();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.GetMatchByGarpOrderBatch()", e);
            }

            return result;
        }

        public static void AddGarUpdateLog(GarpUpdateLog log)
        {
            logger.Info("Running AddGarUpdateLog");
            Console.WriteLine("Running AddGarUpdateLog");

            try
            {
                using (var context = new Models.TexlaEDIContext())
                {
                    context.GarpUpdateLog.Add(log);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in SqlDb.AddGarUpdateLog() : " + e.Message);
                Console.WriteLine("Error in SqlDb.AddGarUpdateLog() : " + e.InnerException.Message);
                logger.Error("Error in SqlDb.AddGarUpdateLog()", e);
            }
        }

        public static List<GarpUpdateLog> GetgarpUpdateLogList(int planproductid)
        {
            List<GarpUpdateLog> result = null;

            logger.Info("Running GetgarpUpdateLogList");

            try
            {
                using (var db = new Models.TexlaEDIContext())
                {
                    result = db.GarpUpdateLog.Where(g => g.PlanProductId == planproductid)?.ToList();
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in SqlDb.GetgarpUpdateLogList()", e);
            }

            return result;
        }

        public static PlanProductReport GetPlanProductSumList(int planFileId, bool shortHorizon)
        {
            PlanProductReport result = new PlanProductReport();
            result.PlanProductSumList = new List<PlanProductSum>();

            using (var context = new Models.TexlaEDIContext())
            {
                List<PlanProduct> planProductList = context.PlanProduct.Where(p => p.PlanFileId == planFileId).ToList();

                PlanFile file = context.PlanFile.Find(planFileId);
                result.File = file.FileName;
                result.Customer = file.CustomerNo + " - " + file.CustomerName;
                result.MessageType = file.MessageType;

                foreach (PlanProduct pp in planProductList)
                {
                    PlanProductSum pps = new PlanProductSum();

                    pps.PlanProductId = pp.Id;
                    pps.ProductNoInFile = pp.ProductNoInFile;
                    pps.ProductNo = pp.ProductNo;
                    pps.ProductName = pp.ProductName;
                    pps.Note = "";

                    IOrderedQueryable<GarpHistoryBatch> garpHistory = null;
                    IOrderedQueryable<GarpOrderBatch> garpOrder = null;
                    List<Match> matches = null;
                    List<PlanDetail> plandetails = null;

                    if (shortHorizon)
                    {
                        DateTime shortHorizonEndDate = DateTime.Now.AddDays(21);

                        garpHistory = context.GarpHistoryBatch.Where(p => p.PlanProductId == pp.Id && p.DeliverDate <= shortHorizonEndDate).OrderBy(o => o.DeliverDate);
                        garpOrder = context.GarpOrderBatch.Where(p => p.PlanProductId == pp.Id && p.DeliverDate <= shortHorizonEndDate).OrderBy(o => o.DeliverDate);
                        matches = context.Match.Where(p => p.PlanProductId == pp.Id && p.Date <= shortHorizonEndDate).OrderBy(m => m.Type).ThenBy(m => m.Date).ToList();
                        plandetails = context.PlanDetail.Where(p => p.PlanProductId == pp.Id && p.DeliverDate <= shortHorizonEndDate).OrderBy(p => p.DeliverDate).ToList();
                    }
                    else
                    {
                        garpHistory = context.GarpHistoryBatch.Where(p => p.PlanProductId == pp.Id).OrderBy(o => o.DeliverDate);
                        garpOrder = context.GarpOrderBatch.Where(p => p.PlanProductId == pp.Id).OrderBy(o => o.DeliverDate);
                        matches = context.Match.Where(p => p.PlanProductId == pp.Id).OrderBy(m => m.Type).ThenBy(m => m.Date).ToList();
                        plandetails = context.PlanDetail.Where(p => p.PlanProductId == pp.Id).OrderBy(p => p.DeliverDate).ToList();
                    }

                    pps.CurrentOrderAmount = garpOrder.Sum(o => o.Amount).HasValue ? garpOrder.Sum(o => o.Amount).Value : 0;
                    pps.CurrentHistoryAmount = garpHistory.Sum(o => o.Amount).HasValue ? garpHistory.Sum(o => o.Amount).Value : 0;
                    pps.CurrentTotAmount = pps.CurrentOrderAmount + pps.CurrentHistoryAmount;

                    pps.PlanAmountBeforeTomorrow = plandetails.Where(p => p.Type == 1).Sum(o => o.Amount).HasValue ? plandetails.Where(p => p.Type == 1).Sum(o => o.Amount).Value : 0;
                    pps.PlanAmount = plandetails.Where(p => p.Type == 2).Sum(o => o.Amount).HasValue ? plandetails.Where(p => p.Type == 2).Sum(o => o.Amount).Value : 0;
                    pps.PlanTotalAMount = pps.PlanAmountBeforeTomorrow + pps.PlanAmount;
                    pps.DiffBeforeMatch = pps.PlanTotalAMount - pps.CurrentTotAmount;

                    pps.MatchAmountAfterUpdate = context.Match.Where(p => p.PlanProductId == pp.Id).Sum(m => m.AmountInGarp).HasValue ? context.Match.Where(p => p.PlanProductId == pp.Id).Sum(m => m.AmountInGarp).Value : 0;

                    pps.SumDiff = (pps.PlanTotalAMount - pps.MatchAmountAfterUpdate);

                    foreach (Match match in matches.Where(m => !string.IsNullOrEmpty(m.Warning)))
                    {
                        pps.Note += match.Warning + " : ";
                    }

                    // Only show them if there is a diff
                    if (pps.DiffBeforeMatch != 0)
                    {
                        result.PlanProductSumList.Add(pps);
                    }
                }

                result.HeaderWarningTotalCount = result.PlanProductSumList.Where(m => !string.IsNullOrEmpty(m.Note)).ToList().Count;
                result.HeaderTotalDiff = result.PlanProductSumList.Sum(o => o.SumDiff);
                result.HeaderTotalChange = result.PlanProductSumList.Sum(o => o.DiffBeforeMatch);

            }

            return result;
        }

        public static PlanProductReport GetAddedOrdersList(int planFileId, List<Match> matchList)
        {
            PlanProductReport result = new PlanProductReport();
            result.PlanProductSumList = new List<PlanProductSum>();

            using (var context = new Models.TexlaEDIContext())
            {
                List<PlanProduct> planProductList = context.PlanProduct.Where(p => p.PlanFileId == planFileId).ToList();

                PlanFile file = context.PlanFile.Find(planFileId);
                result.File = file.FileName;
                result.Customer = file.CustomerNo + " - " + file.CustomerName;
                result.MessageType = file.MessageType;

                foreach (Match match in matchList)
                {
                    PlanProductSum pps = new PlanProductSum();
                    PlanProduct pp = SqlDb.GetPlanProduct(match.PlanProductId.Value);

                    pps.PlanProductId = pp.Id;
                    pps.ProductNoInFile = pp.ProductNoInFile;
                    pps.ProductNo = pp.ProductNo;
                    pps.ProductName = pp.ProductName;
                    pps.Note = match.Note;
                    pps.PlanTotalAMount = match.AmountPlanned.Value;

                    if(string.IsNullOrEmpty(pp.ProductNo))
                    {
                        pps.Warning += "Productno " + pp.ProductNoInFile + " is missing in Garp, this row will not be created";
                    }

                    result.PlanProductSumList.Add(pps);
                }

                //result.HeaderWarningTotalCount = result.PlanProductSumList.Where(m => !string.IsNullOrEmpty(m.Note)).ToList().Count;
                //result.HeaderTotalDiff = result.PlanProductSumList.Sum(o => o.SumDiff);
                //result.HeaderTotalChange = result.PlanProductSumList.Sum(o => o.DiffBeforeMatch);

            }

            return result;
        }
    }
}
