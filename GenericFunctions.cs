﻿using log4net;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace TexlaEDIParser
{
    public static class GenericFunctions
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(Program));
        private static string mDecimalSeparator = "";

        public static int getIntFromStr(string value)
        {
            int result = 0;

            try
            {
                if (!int.TryParse(value, out result))
                {
                    result = 0;
                }
            }
            catch (Exception e)
            {
                logger.Error("Error while converting String to int (" + value + ")", e);
                return 0;
            }

            return result;
        }

        public static string getStrFromInt(Nullable<int> value)
        {
            try
            {
                return Convert.ToString(value);
            }
            catch (Exception e)
            {
                logger.Error("Error while converting Int to String ", e);
                return "0";
            }
        }

        public static string GetSettingsValue(string setting)
        {
            string result = "";

            try
            {
                logger.Debug("Try to read setting: " + setting);

                var rootDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);//System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);

                logger.Debug("Reading settingsfile from: " + rootDir);

                var builder = new ConfigurationBuilder().SetBasePath(rootDir).AddJsonFile("appsettings.json");
                var configuration = builder.Build();

                result = configuration[setting];
            }
            catch (Exception e)
            {
                logger.Error("Error in GenericFunctions.GetSettingsValue() getting setting " + setting, e);
            }

            return result;
        }

        public static decimal getDecimalFromStr(string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return 0;
                }

                if (getCurrentDecimalSeparator().Equals(","))
                    return Convert.ToDecimal(value.Replace(".", ","));
                else
                    return Convert.ToDecimal(value.Replace(",", "."));
            }
            catch (Exception e)
            {
                logger.Error("Error in getDecimalFromStr", e);
                return 0;
            }

        }

        public static string getStrFromDecimal(Nullable<decimal> value)
        {
            try
            {
                if (value.HasValue)
                    return Convert.ToString(value.Value).Replace(",", ".");
                else
                    return "0";
            }
            catch (Exception e)
            {
                logger.Error("Error while converting Decimal to String", e);
                return "0";
            }
        }

        public static DateTime getDateFromStr(string date)
        {
            DateTime result;

            try
            {
                if (string.IsNullOrWhiteSpace(date))
                {
                    result = DateTime.Now;
                }
                else if(date.Length == 6)
                {
                    result = DateTime.ParseExact(date, "yyMMdd", CultureInfo.InvariantCulture);
                }
                else if(date.Length == 8)
                {
                    result = DateTime.ParseExact(date, "yyyyMMdd", CultureInfo.InvariantCulture);
                }
                else
                {
                    logger.Warn("No mathing Date format found, date is: " + date);
                    result = DateTime.Now;
                }
            }
            catch (Exception e)
            {
                logger.Error("Error while converting String (" + date + ")to Date", e);
                result = DateTime.Now;
            }

            return result;
        }

        private static string getCurrentDecimalSeparator()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mDecimalSeparator))
                {
                    System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    mDecimalSeparator = ci.NumberFormat.CurrencyDecimalSeparator;
                }

                return mDecimalSeparator;
            }
            catch (Exception e)
            {
                logger.Error("Error getting CurrentDecimalSeparator", e);
                return mDecimalSeparator;
            }
        }

        public static string getOurReferenceFromMessageType(string messagetype)
        {
            string result = "";

            try
            {
                if(messagetype == "ORDERS")
                {
                    result = "Z1";
                }
                else if (messagetype == "DELINS")
                {
                    result = "Z2";
                }
                if (messagetype == "DELJIT")
                {
                    result = "Z3";
                }
                if (messagetype == "DELFOR")
                {
                    result = "Z4";
                }
            }
            catch (Exception e)
            {
                logger.Error("Error converting MessageType", e);
            }

            return result;
        }
    }
}
