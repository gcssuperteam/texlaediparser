﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TexlaEDIParser.Models;

namespace TexlaEDIParser
{
    public static class FileIOHandler
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(Program));

        public static List<string> GetFileList()
        {
            List<string> result = new List<string>();

            try
            {
                string searchPattern = GenericFunctions.GetSettingsValue("FileSearchPattern");
                string inputFolder = GenericFunctions.GetSettingsValue("EDIFilesInputFolder");

                logger.Debug("FileSearchPattern: " + searchPattern);
                logger.Debug("EDIFilesInputFolder: " + inputFolder);

                result = Directory.GetFiles(inputFolder,searchPattern)?.ToList();
            }
            catch(Exception e)
            {
                logger.Error("Error in FileIOHandler.GetFileList()", e);
            }

            return result;
        }

        public static PlanFile ReadFile(string path)
        {
            PlanFile result = new PlanFile();


            return result;
        }

        public static PlanFile MoveFileToOutput(string filename)
        {
            PlanFile result = new PlanFile();

            try
            {
                string inpath = Path.Combine(GenericFunctions.GetSettingsValue("EDIFilesInputFolder"), filename);
                string outpath = Path.Combine(GenericFunctions.GetSettingsValue("EDIFilesOutputFolder"), filename);

                if (File.Exists(outpath))
                {
                    outpath += "_DUPLICATED_FILE_" + Path.GetRandomFileName();
                }

                File.Move(inpath, outpath);
            }
            catch (Exception e)
            {
                logger.Error("Error in FileIOHandler.MoveFileToOutput(), moving file: " + filename, e);
            }

            return result;
        }
    }
}
