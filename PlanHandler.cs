﻿using GIS_Dto;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TexlaEDIParser.Models;

namespace TexlaEDIParser
{
    public static class PlanHandler
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(Program));

        public static void MatchPlan(PlanProduct plan)
        {
            try
            {
                //if (plan.ProductNo == "318318")
                //    Console.Write("");

                List<GarpHistoryBatch> history = SqlDb.GetGarpHistoryBatchList(plan.Id);
                List<GarpOrderBatch> order = SqlDb.GetGarpOrderBatchList(plan.Id);

                
                List<Match> historyResult = planHistory(plan, history, order);
                List<Match> orderResult = planOrder(plan.Id, order);

                foreach(Match match in historyResult)
                {
                    SqlDb.AddMatch(match);
                }

                foreach (Match match in orderResult)
                {
                    SqlDb.AddMatch(match);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in MatchPlan()", e);
            }
        }

        private static List<Match> planOrder(int planId, List<GarpOrderBatch> order)
        {
            List<Match> result = new List<Match>();

            try
            {
                // Get a new copy of the plan so we can modify PlanDetails list, we want to remove plans before tomorow, those are handeled in planHistory()
                // We also sort PlanDetails in Date order, this is to be sure that all plandetails come in date order, simplifies the match against order (resulting in less change event)
                PlanProduct afterTodayPlan = SqlDb.GetPlanProduct(planId);
                afterTodayPlan.PlanDetail = afterTodayPlan.PlanDetail.Where(p => p.Type == 2).OrderBy(p => p.DeliverDate).ToList();

                // Get a list of "untouchable" orderrows, we deal with these later...
                result.AddRange(extractProductPlanedDates(afterTodayPlan, order));               

                // Get all orderrows that we can change and match them with the plan
                result.AddRange(planOrderChangebleDates(afterTodayPlan, order));

                // If we have more orderrows than plan rows, delete them in Garp.
                result.AddRange(planOrderThatShouldBeDeletedTo(order));

                //result.AddRange(planOrderNotChangebleDates(productPlannedDates));
            }
            catch (Exception e)
            {
                logger.Error("Error in planOrder()", e);
            }

            return result;
        }

        /// <summary>
        /// Match plans that have date today r erlier. This is probably order that already are delivered or delayed.
        /// 2019-11-05: Before we filtered out productionplanned orderrows, but we use them now, just because they are the most plausible to be the right match
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="history"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        private static List<Match> planHistory(PlanProduct plan, List<GarpHistoryBatch> history, List<GarpOrderBatch> order)
        {
            List<Match> result = new List<Match>();

            try
            {
                List<PlanDetail> todayOrErlierPlans = plan.PlanDetail.Where(p => p.DeliverDate.Value.Date <= DateTime.Now.Date)?.ToList();

                // If any plans today or erlier
                if (todayOrErlierPlans?.Count > 0)
                {
                    // All match upp, we are happy
                    if(history.Sum(o=>o.Amount) == todayOrErlierPlans.Sum(p=>p.Amount))
                    {
                        Match match = new Match();

                        match.AmountInGarp = history.Sum(o => o.Amount);
                        match.AmountPlanned = todayOrErlierPlans.Sum(p => p.Amount);
                        match.Date = DateTime.Now;
                        match.ProductNo = plan.ProductNo;
                        match.Action = "";
                        match.Note = "From past to present summarized";
                        match.Type = 1;
                        match.PlanProductId = plan.Id;

                        result.Add(match);
                    }
                    // We have more orders in Garp, something is wrong, add a warning
                    else if(history.Sum(o => o.Amount) > todayOrErlierPlans.Sum(p => p.Amount))
                    {
                        Match match = new Match();

                        match.AmountInGarp = history.Sum(o => o.Amount);
                        match.AmountPlanned = todayOrErlierPlans.Sum(p => p.Amount);
                        match.Date = DateTime.Now;
                        match.ProductNo = plan.ProductNo;
                        match.Note = "From past to present summarized";
                        match.Action = "warn";
                        match.Type = 1;
                        match.PlanProductId = plan.Id;

                        result.Add(match);

                        // Create a new row to even out the mismatch
                        Match matchGetEven = new Match();

                        matchGetEven.PlanProductId = plan.Id;
                        matchGetEven.AmountPlanned = match.AmountPlanned;
                        matchGetEven.AmountInGarp = match.AmountPlanned - match.AmountInGarp;
                        matchGetEven.Date = DateTime.Now.AddDays(1);
                        matchGetEven.Action = "warn";
                        matchGetEven.Note = "Even out history plans";
                        matchGetEven.Type = 2; // Order

                        result.Add(matchGetEven);

                    }
                    // We have more orders in Plan, add row to Garp
                    else if (history.Sum(o => o.Amount) < todayOrErlierPlans.Sum(p => p.Amount))
                    {
                        Match match = new Match();

                        match.AmountInGarp = history.Sum(o => o.Amount);
                        match.AmountPlanned = todayOrErlierPlans.Sum(p => p.Amount);
                        match.Date = DateTime.Now;
                        match.ProductNo = plan.ProductNo;
                        match.Note = "From past to present summarized";
                        match.Action = "warn";
                        match.Type = 1;
                        match.PlanProductId = plan.Id;

                        result.Add(match);

                        Match matchEqual = new Match();

                        // If we got som orderrows availible, we use the first one.
                        //if (order.Where(o => o.IsProductPlanned.Value != true)?.ToList().Count > 0)
                        if (order.Count > 0)
                        {
                            //GarpOrderBatch foundOrder = order.Where(o => o.IsProductPlanned.Value != true).First();
                            GarpOrderBatch foundOrder = order.First();

                            // If this row is productplanned and not the same amount, add a diffpost to the order
                            if(foundOrder.IsProductPlanned == true && foundOrder.Amount.Value != todayOrErlierPlans.Sum(p => p.Amount).Value)
                            {
                                matchEqual.AmountInGarp = history.Sum(o => o.Amount) - foundOrder.Amount;
                                matchEqual.Date = DateTime.Now;
                                matchEqual.ProductNo = plan.ProductNo;
                                matchEqual.Note = "Equal out prod.planned order";
                                matchEqual.Action = "add";
                                matchEqual.Type = 1;
                                matchEqual.PlanProductId = plan.Id;
                                matchEqual.GarpOrderBatchId = foundOrder.Id;
                                matchEqual.PlanDetailId = todayOrErlierPlans.First()?.Id;
                                matchEqual.Warning = "Correction added to product planned order, check this order!";
                            }
                            //  This row was not productplanned or the amount was the same, then we change the orderrow
                            else
                            {
                                matchEqual.AmountInGarp = todayOrErlierPlans.Sum(p => p.Amount) - history.Sum(o => o.Amount);
                                matchEqual.ProductNo = plan.ProductNo;
                                matchEqual.Note = "New post to equal out past plans";
                                matchEqual.Date = DateTime.Now;
                                matchEqual.Type = 1;
                                matchEqual.PlanProductId = plan.Id;
                                matchEqual.GarpOrderBatchId = foundOrder.Id;

                                // Determine if we doing a change of amount or just a change of date
                                if (foundOrder.Amount != todayOrErlierPlans.Sum(p => p.Amount).Value)
                                {
                                    matchEqual.Action = "change";
                                }
                                else
                                {
                                    matchEqual.Action = "change_date";
                                }
                            }

                            order.Remove(foundOrder);
                        }
                        else
                        {
                            matchEqual.AmountInGarp = todayOrErlierPlans.Sum(p => p.Amount) - history.Sum(o => o.Amount);
                            matchEqual.Date = DateTime.Now;
                            matchEqual.ProductNo = plan.ProductNo;
                            matchEqual.Note = "New post to equal out past plans";
                            matchEqual.Action = "add";
                            matchEqual.Type = 1;
                            matchEqual.PlanProductId = plan.Id;
                        }

                        result.Add(matchEqual);
                    }
                }
                else if(history.Count > 0) // If no plans today or erlier, BUT we have orders in Garp today or erlier, add a warning
                {
                    Match match = new Match();

                    match.AmountInGarp = history.Sum(o => o.Amount);
                    match.AmountPlanned = todayOrErlierPlans.Sum(p => p.Amount);
                    match.Date = DateTime.Now;
                    match.ProductNo = plan.ProductNo;
                    match.Note = "From past to present summarized";
                    match.Action = "";
                    //match.Warning = "No plans today or erlier but order exists in Garp";
                    //match.Warning = "Garp has " + match.AmountInGarp.ToString() + " delivered that shouldn't be delivered at customer yet, but still is missing in the planfile";
                    match.Type = 1;
                    match.PlanProductId = plan.Id;

                    result.Add(match);

                    // Create a new row to even out the mismatch
                    Match matchGetEven = new Match();

                    matchGetEven.PlanProductId = plan.Id;
                    matchGetEven.AmountPlanned = match.AmountPlanned;
                    matchGetEven.AmountInGarp = match.AmountPlanned - match.AmountInGarp;
                    matchGetEven.Date = DateTime.Now.AddDays(1);
                    matchGetEven.Note = "Found history orders in Garp (" + match.AmountInGarp + ")  but nothing i planfile";
                    matchGetEven.Action = "warn";
                    matchGetEven.Type = 1; // Order

                    result.Add(matchGetEven);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in planHistory()", e);
            }

            return result;
        }

        /// <summary>
        /// Find all product planned orderrows on this product in garp ,and get all planning on same date and return them as a lsit of Match records.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        private static List<Match> extractProductPlanedDates(PlanProduct plan, List<GarpOrderBatch> order)
        {
            List<Match> result = new List<Match>();

            try
            {
                // Get list of all productpalnned order
                var planned = order.Where(o => o.IsProductPlanned == true);

                if(planned.Count() > 0)
                {
                    foreach (GarpOrderBatch gob in planned)
                    {
                        Match match = new Match();

                        match.AmountInGarp = gob.Amount;
                        match.Date = gob.DeliverDate;
                        match.GarpOrderBatchId = gob.Id;
                        match.ProductNo = gob.ProductNo;
                        match.PlanProductId = plan.Id;
                        match.Note = "product planned";
                        match.Type = 2;

                        // JB 2021-06-14: Removed this to fix error that cause productplanned orderrows to be delted
                        //if (order.Remove(gob) != true)
                        //{
                        //    logger.Debug("Could not remove planned order: " + JsonConvert.SerializeObject(gob));
                        //}


                        // Get the first plan from PlanDetails. If we have plans the first on i almost always the on
                        // that are most likley to match a productplanned orderrow (production planning are not made that far in the future)
                        PlanDetail plandetail = null;
                        try
                        {
                            plandetail = plan.PlanDetail?.First();//plan.PlanDetail?.First(p => p.DeliverDate.Value.Date == gob.DeliverDate.Value.Date); 
                        }
                        catch { }

                        
                        // If we found a plandetail
                        if(plandetail != null)
                        {
                            match.AmountPlanned = plandetail.Amount;
                            match.PlanDetailId = plandetail.Id;
                            match.Action = "";

                            // 2019-09-23: Added this after sugestion from Per. Date is only changed on order not in production plan.
                            // 2021-03-17: Changed back, request from Ulrica / Texla
                            //if (plandetail.DeliverDate.Value.Date != gob.DeliverDate.Value.Date)
                            //{
                            //    match.Action = "change_date";
                            //    match.Date = plandetail.DeliverDate.Value.Date;
                            //}
                            //else
                            //{
                            //    match.Action = "";
                            //}

                            plan.PlanDetail.Remove(plandetail);
                        }
                        else
                        {
                            match.AmountPlanned = 0;
                        }

                        result.Add(match);

                        // If planned and amount in Garp not match, get an diff post to equal it out
                        if (match.AmountInGarp != match.AmountPlanned)
                        {
                            result.Add(getNewDiffMatch(gob, plandetail, plan.Id));
                        }
                    }

                }
            }
            catch(Exception e)
            {
                logger.Error("Error in extractProductPlanedDates()", e);
            }

            try
            {
                // Remove all productplanned orders from ordinary orderlist
                order.RemoveAll(o => o.IsProductPlanned == true);
            }
            catch (Exception e)
            {
                logger.Error("Error in extractProductPlanedDates() while removing order that are productplanned", e);
            }

            return result;
        }

        private static Match getNewDiffMatch(GarpOrderBatch garpOrderBatch, PlanDetail plandetail, int planproductid)
        {
            Match match = new Match();

            try
            {
                match.Date = plandetail.DeliverDate;
                match.GarpOrderBatchId = garpOrderBatch.Id;
                match.ProductNo = garpOrderBatch.ProductNo;
                match.PlanProductId = planproductid;
                match.Action = "add";
                match.Note = "diff";
                match.Type = 2;
                match.Warning = "diff";

                if (plandetail != null)
                {
                    match.AmountInGarp = plandetail.Amount - garpOrderBatch.Amount;
                    match.PlanDetailId = plandetail.Id;
                    match.Note = "Planned amount is " + plandetail.Amount + " and amount in Garp is " + garpOrderBatch.Amount;
                    match.Warning = "Planned amount is " + plandetail.Amount + " and amount in Garp is " + garpOrderBatch.Amount;
                }
                else
                {
                    match.AmountInGarp = garpOrderBatch.Amount * -1; // We have to withdraw this amount from Garp
                    match.Note = "Plan on this date is missing, but amount in Garp is " + garpOrderBatch.Amount;
                    match.Warning = "Plan on this date is missing, but amount in Garp is " + garpOrderBatch.Amount;
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in addPlanAndOrderToMatch()", e);
            }

            return match;
        }

        private static List<Match> planOrderChangebleDates(PlanProduct plan, List<GarpOrderBatch> order)
        {
            List<Match> result = new List<Match>();

            try
            {
                foreach(PlanDetail plandetail in plan.PlanDetail)
                {
                    Match match = getPlanAndOrderToMatch(plandetail, (order.Count == 0 ? null :  order[0]), plan.ProductNo);
                    result.Add(match);

                    if (order.Count > 0)
                    {
                        // If exists we remove this order from list, we are done with it now
                        order.Remove(order[0]);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in planOrderChangebleDates()", e);
            }

            return result;
        }

        private static Match getPlanAndOrderToMatch(PlanDetail plandetail, GarpOrderBatch garpOrderBatch, string productno)
        {
            Match match = new Match();

            try
            {
                match.PlanProductId = plandetail.PlanProductId;
                match.AmountPlanned = plandetail.Amount;
                match.AmountInGarp = plandetail.Amount;
                match.PlanDetailId = plandetail.Id;
                match.Date = plandetail.DeliverDate;
                match.Type = 2; // Order

                if(garpOrderBatch != null)
                {
                    match.GarpOrderBatchId = garpOrderBatch.Id;
                    match.ProductNo = garpOrderBatch.ProductNo;

                    if(plandetail.Amount != garpOrderBatch.Amount)
                    {
                        match.Action = "change";
                    }
                    else if(plandetail.DeliverDate.Value.Date != garpOrderBatch.DeliverDate.Value.Date)
                    {
                        match.Action = "change_date";
                    }
                    else
                    {
                        match.Action = "";
                    }
                }
                else
                {
                    match.AmountInGarp = plandetail.Amount;
                    match.Date = plandetail.DeliverDate;
                    match.ProductNo = productno;
                    match.Action = "add";
                }

                if(string.IsNullOrEmpty(match.ProductNo))
                {
                    match.Warning = "Product is missing in Garp";
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in addPlanAndOrderToMatch()", e);
            }

            return match;
        }

        private static List<Match> planOrderThatShouldBeDeletedTo(List<GarpOrderBatch> garpOrderBatchList)
        {
            List<Match> result = new List<Match>();

            try
            {
                foreach(GarpOrderBatch gob in garpOrderBatchList)
                {
                    Match match = new Match();

                    match.PlanProductId = gob.PlanProductId;
                    match.AmountInGarp = 0;
                    match.AmountPlanned = 0;
                    match.Date = gob.DeliverDate;
                    match.GarpOrderBatchId = gob.Id;
                    match.Action = "delete";
                    match.Type = 2;

                    result.Add(match);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in addPlanAndOrderToMatch()", e);
            }

            return result;
        }
    }
}
