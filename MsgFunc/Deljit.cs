﻿using GIS_Dto;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TexlaEDIParser.Models;

namespace TexlaEDIParser.MsgFunc
{
    public static class Deljit
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(Program));

        public static void runDeljit(PlanFile planfile)
        {
            try
            {
                List<Match> addedMatchToOrderList = new List<Match>();

                foreach (PlanProduct product in planfile.PlanProduct)
                {
                    // Get a list of Matches created from PlanProduct
                    List<Match> matches = getMatchList(product);

                    // Add list of mathces to Garp
                    addedMatchToOrderList.AddRange(addOrderRows(matches));
                }

                // When done, move file to outputfolder
                FileIOHandler.MoveFileToOutput(planfile.FileName);

                PlanProductReport report = SqlDb.GetAddedOrdersList(planfile.Id, addedMatchToOrderList);
                sendMail(report);

            }
            catch(Exception e)
            {
                logger.Error("Error in createOrder", e);
            }
        }

        private static List<Match> addOrderRows(List<Match> matches)
        {
            List<Match> result = new List<Match>();

            try
            {
                foreach (Match match in matches)
                {
                    // If ProductNo is missing dont try to add row
                    if(!string.IsNullOrEmpty(match.ProductNo))
                    {
                        OrderHead oh = GarpDb.AddOrderrow(match);
                        match.Note = oh.OrderNo;
                    }

                    result.Add(match);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in createOrder", e);
            }

            return result;
        }

        //private static OrderHead getOrderObjectForProduct(PlanProduct product, string customerNo, string messagetype)
        //{
        //    OrderHead result = new OrderHead();

        //    try
        //    {
        //        result.DeliverCustomerNo = customerNo;
        //        result.OurReferenceId = GenericFunctions.getOurReferenceFromMessageType(messagetype);
        //    }
        //    catch(Exception e)
        //    {
        //        logger.Error("Error in getOrderObjectForProduct", e);
        //    }

        //    return result;
        //}

        private static List<Match> getMatchList(PlanProduct plan)
        {
            List<Match> result = new List<Match>();

            try
            {
                foreach (PlanDetail plandetail in plan.PlanDetail)
                {
                    Match match = getMatch(plandetail, plan.ProductNo);
                    result.Add(match);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in planOrderChangebleDates()", e);
            }

            return result;
        }

        private static Match getMatch(PlanDetail plandetail, string productno)
        {
            Match match = new Match();

            try
            {
                //match.Id = null;
                match.PlanProductId = plandetail.PlanProductId;
                match.AmountPlanned = plandetail.Amount;
                match.AmountInGarp = plandetail.Amount;
                match.PlanDetailId = plandetail.Id;
                match.Date = plandetail.DeliverDate;
                match.Type = 2; // Order

                match.AmountInGarp = plandetail.Amount;
                match.Date = plandetail.DeliverDate;
                match.ProductNo = productno;
                match.Action = "add";
            }
            catch (Exception e)
            {
                logger.Error("Error in getMatch()", e);
            }

            return match;
        }

        private static void sendMail(PlanProductReport report)
        {
            SendMailParam param = new SendMailParam();

            param.FromEmail = GenericFunctions.GetSettingsValue("MailSender");
            param.FromName = "EDI";
            param.Subject = "EDI-fil ( " + report.File + " ) inläst";

            try
            {
                param.To = JsonConvert.DeserializeObject<List<SendMailReceipent>>(GenericFunctions.GetSettingsValue("MailReceipents"));
            }
            catch (Exception e)
            {
                logger.Error("Error in SendMail while getting receipents", e);
            }
            param.MessageHtml = File.ReadAllText(GenericFunctions.GetSettingsValue("DeljitMailTemplate"));

            StringBuilder sbTR = new StringBuilder();

            foreach (PlanProductSum sum in report.PlanProductSumList.OrderBy(p => p.ProductNo))
            {
                sbTR.Append("<tr>");

                sbTR.Append("<td>");
                sbTR.Append(sum.Note);
                sbTR.Append("</td>");

                sbTR.Append("<td>");
                sbTR.Append(sum.ProductNo);
                sbTR.Append("</td>");

                sbTR.Append("<td>");
                sbTR.Append(sum.ProductNoInFile);
                sbTR.Append("</td>");

                sbTR.Append("<td>");
                sbTR.Append(sum.ProductName);
                sbTR.Append("</td>");

                sbTR.Append("<td>");
                sbTR.Append(sum.PlanTotalAMount);
                sbTR.Append("</td>");

                sbTR.Append("<td>");
                sbTR.Append(sum.Warning);
                sbTR.Append("</td>");

                sbTR.Append("</tr>");
            }

            param.MessageHtml = param.MessageHtml.Replace("{HEADER}", report.MessageType);
            param.MessageHtml = param.MessageHtml.Replace("{FILE}", report.File);
            param.MessageHtml = param.MessageHtml.Replace("{CUSTOMER}", report.Customer);
            param.MessageHtml = param.MessageHtml.Replace("{WARNINGS}", report.HeaderWarningTotalCount.ToString());
            param.MessageHtml = param.MessageHtml.Replace("{CHANGES}", report.HeaderTotalChange.ToString());
            param.MessageHtml = param.MessageHtml.Replace("{ROWS}", sbTR.ToString());

            GIS.GISCalls.SendMail(param);

        }
    }
}
