﻿using GIS_Dto;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TexlaEDIParser.Models;

namespace TexlaEDIParser.MsgFunc
{
    public static class Delfor
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(Program));

        public static void runDelfor(PlanFile planfile)
        {
            List<string> fileList = new List<string>();

            try
            {
                // Add all corresponding order in this planfile to SqlDb
                GarpDb.AddGarpOrderList(planfile);

                // Add all corresponding history in this planfile to SqlDb
                GarpDb.AddGarpHistoryList(planfile);

                // Execute plans in current file
                matchPlansInFile(planfile);

                // When done, move file to outputfolder
                FileIOHandler.MoveFileToOutput(planfile.FileName);

                // Execute all gadered EDI data to Garp
                ExecuteAllNotAlreadyExecutedPlanFilesOnGarp();

                if (GenericFunctions.GetSettingsValue("MailReceipents") != "")
                {
                    PlanProductReport report = SqlDb.GetPlanProductSumList(planfile.Id, true);
                    sendMail(report);
                }

            }
            catch (Exception e)
            {
                logger.Error("Error in BusinessLayer.runMatch()", e);
            }
        }

        public static void ExecutePlanFileOnGarp(int planfileid)
        {
            try
            {
                PlanFile pf = SqlDb.GetPlanFile(planfileid);
                Console.WriteLine("Got planfile with id " + pf.Id);

                if (pf.UpdatedToGarp != true)
                {
                    foreach (PlanProduct pp in pf.PlanProduct)
                    {
                        if (SqlDb.GetgarpUpdateLogList(pp.Id)?.Count == 0)
                        {
                            GarpDb.ExecuteMatchOnGarp(pp.Id);
                        }
                    }

                    // Set this planfile as updated to Garp so we dont execute it several times.
                    pf.UpdatedToGarp = true;
                    SqlDb.UpdatePlanFile(pf);
                }
                else
                {
                    logger.Debug("Got planfile " + pf.Id + " but this was already updated to Garp so we skip this one");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error in BusinessLayer.updatePlanToGarp() : " + e.Message);
                logger.Error("Error in BusinessLayer.updatePlanToGarp()", e);
            }
        }

        /// <summary>
        /// Go back x days and execute all planproduct thats not salready executed against Garp
        /// 
        /// </summary>
        public static void ExecuteAllNotAlreadyExecutedPlanFilesOnGarp()
        {
            try
            {
                List<PlanFile> pfList = SqlDb.GetAllPlanFilesNotUpdatedToGarp();

                foreach (PlanFile file in pfList)
                {
                    ExecutePlanFileOnGarp(file.Id);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error in BusinessLayer.updatePlanToGarp()", e);
            }
        }

        private static void matchPlansInFile(PlanFile planfile)
        {
            try
            {
                // Loop every plan on product in this file and execute that plan
                foreach (PlanProduct planproduct in planfile.PlanProduct)
                {
                    matchPlanOnProduct(planproduct, planfile.CustomerNo);
                }

            }
            catch (Exception e)
            {
                logger.Error("Error in BusinessLayer.executePlansInFile()", e);
            }
        }

        /// <summary>
        /// Execute a plan on the product level
        /// </summary>
        /// <param name="planproduct"></param>
        /// <param name="customer"></param>
        private static void matchPlanOnProduct(PlanProduct planproduct, string customerNo)
        {
            try
            {
                PlanHandler.MatchPlan(planproduct);
            }
            catch (Exception e)
            {
                logger.Error("Error in BusinessLayer.executePlanOnProduct()", e);
            }
        }


        private static void sendMail(PlanProductReport report)
        {
            SendMailParam param = new SendMailParam();

            param.FromEmail = GenericFunctions.GetSettingsValue("MailSender");
            param.FromName = "EDI";
            param.Subject = "EDI-fil ( " + report.File + " ) inläst";

            try
            {
                param.To = JsonConvert.DeserializeObject<List<SendMailReceipent>>(GenericFunctions.GetSettingsValue("MailReceipents"));
            }
            catch (Exception e)
            {
                logger.Error("Error in SendMail while getting receipents", e);
            }
            param.MessageHtml = File.ReadAllText(GenericFunctions.GetSettingsValue("DelforMailTemplate"));

            StringBuilder sbTR = new StringBuilder();

            foreach (PlanProductSum sum in report.PlanProductSumList.OrderBy(p => p.ProductNo))
            {
                sbTR.Append("<tr>");

                sbTR.Append("<td>");
                sbTR.Append(sum.ProductNo);
                sbTR.Append("</td>");

                sbTR.Append("<td>");
                sbTR.Append(sum.ProductNoInFile);
                sbTR.Append("</td>");

                sbTR.Append("<td>");
                sbTR.Append(sum.ProductName);
                sbTR.Append("</td>");

                sbTR.Append("<td>");
                sbTR.Append(sum.CurrentTotAmount);
                sbTR.Append("</td>");

                sbTR.Append("<td>");
                sbTR.Append(sum.PlanTotalAMount);
                sbTR.Append("</td>");

                sbTR.Append("<td>");
                sbTR.Append(sum.DiffBeforeMatch);
                sbTR.Append("</td>");

                sbTR.Append("<td>");
                sbTR.Append(sum.Note);
                sbTR.Append("</td>");

                sbTR.Append("</tr>");
            }

            param.MessageHtml = param.MessageHtml.Replace("{HEADER}", report.File.ToString());
            param.MessageHtml = param.MessageHtml.Replace("{FILE}", report.File.ToString());
            param.MessageHtml = param.MessageHtml.Replace("{CUSTOMER}", report.Customer.ToString());
            param.MessageHtml = param.MessageHtml.Replace("{WARNINGS}", report.HeaderWarningTotalCount.ToString());
            param.MessageHtml = param.MessageHtml.Replace("{CHANGES}", report.HeaderTotalChange.ToString());
            param.MessageHtml = param.MessageHtml.Replace("{ROWS}", sbTR.ToString());

            GIS.GISCalls.SendMail(param);


            //string rest = "";

            //try
            //{
            //    rest = "http://localhost:81/GIS/MailSvc/REST/SendMail/";


            //    var request = WebRequest.Create(rest);
            //    request.Method = "POST";
            //    request.ContentType = "application/json; charset=utf-8";

            //    using (var writer = new StreamWriter(request.GetRequestStream()))
            //    {
            //        writer.Write(JsonConvert.SerializeObject(param));
            //    }

            //    using (var response = request.GetResponse())

            //    using (var reader = new StreamReader(response.GetResponseStream()))
            //    {
            //        string result = reader.ReadToEnd();
            //        // do something with the results
            //    }
            //}
            //catch (Exception e)
            //{

            //}
        }
    }
}
